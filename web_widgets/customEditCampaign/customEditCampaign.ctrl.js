 /// angular 1 form
function ($scope, $http, $window, $timeout, $filter ) {
     $scope.tabName = 'tab1';
     $scope.isForm = true;
     $scope.isPlacementSlctd = false;
  
     $scope.filesVar =   new FormData();
    $scope.campaign = {};
    $scope.crSize = 10;	
    $scope.actualcrSizeAPI = 0;	
    $scope.detailsSubmit = false;
    $scope.products = [];
    $scope.agencies = [];
    $scope.advertizers = [];
    $scope.primaryCandidates = [];
    $scope.adTypes = [];
    $scope.airingChannels = [];
    $scope.masterDataAiringchannels = {};
    $scope.creativeList = [];
    $scope.campaignCreatives = [];
    $scope.placement = '';
     $scope.creative={
         creativeId:''
     }
     
     $scope.sizevvv = 0;	
     $scope.creativeObj = [];	
     $scope.airingChnlList = [];
     $scope.toDateError = false;	
     $scope.fromDateError = false;	
     $scope.clientRefError = false;	
     $scope.advertisError = false;	
     $scope.primeCandidatError = false;	
     $scope.adTypError = false;	
     $scope.telecastError = false;	
     $scope.fileError = false;	
     $scope.airingChannelError = false;	
     $scope.placementError = false;	
     $scope.clneCreativeList = [];
        $scope.campaign={}
   
        $scope.airingChannelsList = [];
     $scope.zIndex = 0;
      $scope.zIndexP = 0;
    let apiHostName = "http://localhost:9004";
    $scope.creativeLLength = 0;
   //      $("#uid_primaryCandidate").show();

   
      $scope.init = function () {
       console.log("campaign---",$scope.properties.campaign)
            $scope.properties.campaign.startDate = new Date($scope.properties.campaign.startDate);
            $scope.properties.campaign.endDate = new Date($scope.properties.campaign.endDate);
        $http.get(apiHostName+"/api/v1/adTypes", {})
                .then(function (response) {
                    let res = response.data;
                    $scope.adTypes = angular.copy(res.payload);
                    
                });
                
        $http.get(apiHostName+"/api/v1/airingChannels", {})
                .then(function (response) {
                    let res = response.data;
                    $scope.airingChannels = angular.copy(res.payload);
                    $scope.masterDataAiringchannels = JSON.stringify($scope.airingChannels);
                })
                  $scope.getCreativeList();
   }
   
  	
                	

   	
   $scope.getCreativeList = function(){
        $http.get(apiHostName+"/api/v1/creatives", {})	
            .then(function (response) {	
            let res = response.data;	
            $scope.creativeObj = angular.copy(res.payload);	
            $scope.clneCreativeList = $scope.creativeObj;	
            $scope.actualcrSizeAPI = $scope.creativeObj.length; console.log($scope.actualcrSizeAPI);	
            if($scope.actualcrSizeAPI < $scope.crSize) { // console.log($scope.crSize, '$scope.crSize')	
                $scope.crSize = $scope.actualcrSizeAPI;	
            }	
        })	
   }	
   	
   $scope.loadMore = function() {	
       $scope.crSize = $scope.crSize + 10;	
       if($scope.actualcrSizeAPI < $scope.crSize) { // console.log($scope.crSize, '$scope.crSize')	
                $scope.crSize = $scope.actualcrSizeAPI;	
        } 	
   }
   
    $scope.getAgency = function() {
       
         console.log($scope.campaign.agency, '$scope.campaign.agency');
          $http.get(apiHostName+"/api/v1/customerByName?accessType=Agency&name="+$scope.campaign.agency, {})
                .then(function (res) {
                    console.log( 'Agency', res);
                    response = res.data;
                    // console.log(response.body.payload, 'Agency');
                    $scope.agencies = angular.copy(response.payload);
                    
                });
    };
    
    $scope.onSelectAgency = function(agencyname){
      
       $scope.campaign.agency = '';
       $scope.campaign.agency = agencyname.longName;
       $("#uid_agency").hide();
    };
    
    $scope.getAdvertiserNotUsed = function(e) {
       
         console.log($scope.properties.campaign.advertiser, '$scope.properties.campaign.advertiser');
          $http.get(apiHostName+"/api/v1/customerByName?accessType=Advertiser&name="+$scope.campaign.advertiser, {})
                .then(function (res) {
                    console.log( 'Agency', res);
                    response = res.data;
                    // console.log(response.body.payload, 'Agency');
                    $scope.advertizers = angular.copy(response.payload);
                    
                });
   }
    $scope.addCreatives = function(){	
      // console.log($scope.creativeList.length, '$scope.creativeList.length', Object.keys($scope.creativeList).length);	
       $scope.creative={	
         creativeId:''	
        }	
         for (var i = 0; i < $scope.creativeList.length; i++) {	
              $scope.creativeList[i] ;	
           	
                   $scope.campaignCreatives.push( { creativeId: $scope.creativeList[i]});	
         }	
         $scope.creativeLLength = $scope.creativeList.length;	
   }
   

$scope.zIndexP = 0;   
 var  typingA = false;
     $scope.getAdvertiser = function(e){
     $scope.zIndex = 0;  
    if(!typingA){
          typingA = true;
          $scope.enFlag = false;
          console.log(typingA);
              $timeout(function(){
                  typingA = false;
                            $scope.advertizers = [];
                         
                            $http.get(apiHostName+"/api/v1/customerByName?accessType=Advertiser&name="+ $scope.properties.campaign.advertiserName, {})
                .then(function (res) {
                   console.log( 'Agency', res);
                    response = res.data;
                      if( res.data){
                                 $scope.zIndex = 1;  
                                 $scope.advertizers = angular.copy(response.payload);
                                  if( $scope.advertizers.payloadCount === 0){
                                         console.log("No primaryCandidates ");
                                      $scope.zIndex = 0;  
                                  }
                      }else{
                               $scope.properties.campaign.advertiserName = "";
                            }
              },function(error){
                                  $scope.zIndex = 0;  
                        });
              },1000)
              }
}
   $scope.onSelectAdvertiser = function(advname){
        console.log(advname, 'advname');
          $scope.properties.campaign.advertiserName = '';
          $scope.properties.campaign.advertiserName = advname.longName;
        $("#uid_advertisers").hide();
   }
   
   
    $scope.zIndexP = 0;   
 var  typingB = false;
 
 
  $scope.getPrimaryCandidates = function(e){
     $scope.zIndexP = 0;  
    if(!typingB){
          typingB = true;
          $scope.enFlagNew = false;
          console.log(typingB);
              $timeout(function(){
                  typingB = false;
                            $scope.primaryCandidates = [];
                         
                            $http.get(apiHostName+"/api/v1/candidates/"+$scope.properties.campaign.primaryCandidateName, {})
                .then(function (res) {
                    console.log( 'candidates', res);
                    response = res.data;
                      if( res.data){
                                 $scope.zIndexP = 1;  
                                 $scope.primaryCandidates = angular.copy(response.payload);
                                  if( $scope.primaryCandidates.payloadCount == 0){
                                         console.log("No primaryCandidates ");
                                      $scope.zIndexP = 0;  
                                  }
                      }else{
                               $scope.properties.campaign.primaryCandidateName = "";
                            }
              },function(error){
                                  $scope.zIndexP = 0;  
                        });
              },1000)
              }
}
     
   $scope.onSelectPrimaryCandidate = function(primaryCandidate){
        console.log(primaryCandidate, 'primaryCandidate');
        $scope.properties.campaign.primaryCandidateName = '';
        $scope.properties.campaign.primaryCandidateName= primaryCandidate.candidateName;
        $("#uid_primaryCandidate").hide();
   }
   
   
   
   $scope.airingChannelFilter = function(sletters) {
       let channels = JSON.stringify(JSON.parse($scope.masterDataAiringchannels));
       if(sletters === 'all') {
           filtered = JSON.parse(channels)
       } else {
           letters = sletters.split(',')
        var filtered = [];
        items = JSON.parse(channels)
        console.log(items, 'items', letters)
        for (var l = 0; l < letters.length; l++) {
        var letterMatch = new RegExp(letters[l], 'i');
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              if (letterMatch.test(item.channelName.substring(0, 1))) {
                filtered.push(item);
              }
            }
        }
       }
        $scope.airingChannels = filtered;
    };
    
    $scope.onSelectImg = function(cid) {	
       console.log(cid, 'cid');	
        if(Object.keys($scope.creativeList).length === 0) {	
          $scope.creativeList.push(cid);	
        } else {	
            let isExi = $scope.creativeList.findIndex(c => c === cid);	
            console.log(isExi, 'is exists', $scope.creativeList)	
            if(isExi == -1){	
                 $scope.creativeList.push(cid);console.log($scope.creativeList.length, 'innn')	
            } else {	
                 $scope.creativeList.splice(isExi, 1);	
            }	
        }	
                console.log($scope.creativeList.length, '$scope.creativeList.length', Object.keys($scope.creativeList).length);	
    }	
    	
    $scope.clearAllCreatives = function() {
        	console.log($scope.creativeList , '$scope.creativeList')
        for (let i = 0; i < $scope.creativeList.length; i++) {	
             let isExi = $scope.creativeList.findIndex(c => c);	
            console.log($scope.creativeList[i], 'list');	
             $scope.creativeList.splice(isExi, 1);	
             // $scope.creativeList.splice(i, 1);	
         }	
         	$scope.creativeList = [];

	
    }	
     $scope.selectedRow = function(e) {
        if($scope.sizevvv != 4){	
            this.crSize = $scope.sizevvv;	
        } else {	
            this.crSize = 10;	
        }	
    }
       
    $scope.onSelectAiringChannel = function(name) {
       
         if(Object.keys($scope.airingChannelsList).length === 0) {
              $scope.airingChannelsList.push(name);
         } else {
            let isExi = $scope.airingChannelsList.findIndex(c => c === name);
            if(isExi == -1){
                 $scope.airingChannelsList.push(name);
            } else {
                 $scope.airingChannelsList.splice(isExi, 1);
            }
         }
         
          console.log($scope.airingChannelsList, 'airingChannelsList');
    }

   
     $scope.reloadDashBoard = function () {
        $window.parent.location.reload();
    }
    var URL = "/bonita/portal/resource/process/Campaign Process/1.1/API/bpm/process/" + $scope.properties.processId + "/instantiation"
     
     
     
       $scope.submitTask = function () {
        
         
      var id;
        id = $scope.properties.taskId ;
        
     $scope.properties.campaign.endDate =  $filter('date')(new Date( $scope.properties.campaign.endDate),'dd/MM/yyyy');
         $scope.properties.campaign.startDate =  $filter('date')(new Date( $scope.properties.campaign.startDate),'dd/MM/yyyy');
         $scope.properties.campaign.agency =$scope.properties.userId ;
         var airingChannel =""
          for (var i = 0; i < $scope.airingChannelsList.length; i++) {
              if(airingChannel === ""){
                 airingChannel =  $scope.airingChannelsList[i] ;
              }else{
                      airingChannel = airingChannel+","+$scope.airingChannelsList[i];
              }
         
          }
      $scope.properties.campaign.placementFrom = $scope.placement ;
      $scope.properties.campaign.agency = $scope.properties.userId;
     $scope.properties.campaign.airingChannel = airingChannel;
       $scope.properties.campaign.creatives = $scope.campaignCreatives
       console.log("Final campaign ::", campaign);
        var request = {
            "campaignInput":  $scope.properties.campaign
        }
        console.log(request);
        if (id) {
            $http.put('../API/bpm/humanTask/' + id, { 'assigned_id': $scope.properties.user.userId }).then(function (response) {
                $http.post('../API/bpm/userTask/' + id + '/execution', request, { 'user': $scope.properties.user.userId }).then(function (response) {
                }, function (error) {
                });
            }, function (error) {
                //console.log(error);
            });
        } else {
            console.log('Impossible to retrieve the task id value from the URL');
        }
    }
       
    
 $scope.saveCampaign = function (campaign,userID) {
     console.log("$scope.files ---",$scope.files )
     $scope.properties.campaign.endDate =  $filter('date')(new Date( $scope.properties.campaign.endDate),'dd/MM/yyyy');
         $scope.properties.campaign.startDate =  $filter('date')(new Date( $scope.properties.campaign.startDate),'dd/MM/yyyy');
         $scope.properties.campaign.agency =userID ;
         var airingChannel =""
          for (var i = 0; i < $scope.airingChannelsList.length; i++) {
              if(airingChannel === ""){
                 airingChannel =  $scope.airingChannelsList[i] ;
              }else{
                      airingChannel = airingChannel+","+$scope.airingChannelsList[i];
              }
         
          }
       campaign.placementFrom = $scope.placement ;
       campaign.agency = $scope.properties.userId;
       campaign.airingChannel = airingChannel;
       campaign.creatives = $scope.campaignCreatives
       console.log("Final campaign ::", campaign);
        var request = {
            "campaignInput": campaign,
        }
        console.log(request);
        //Rest call for create mandate
        $http.post(URL, request)
            .then(function (response) {
                $scope.submitDocuments(response.data.caseId);
            }, function (error) {
                console.log(error);
            });
    }
    

   
       $scope.submitDocuments = function (caseId) {
           console.log("caseId---",caseId);
               console.log("$scope.filesVar ---", $scope.filesVar  )
                 console.log("$scope.files.length ---",$scope.files.length )
    
            if (caseId  ) {
               //  var documentsLength = $scope.files.length;
                var count = 0; 
          
                    
                 //   var doc = $scope.files;
                  //  var formData = new FormData();
                  //   $scope.filesVar .append('file', doc);
                     $scope.filesVar.append('sourceId', caseId);
                     $scope.filesVar.append('sourceType', "Campaign");
                     $scope.filesVar.append('userName', $scope.properties.userId);
                     $scope.filesVar.append('details',"Campaign_details");
                     $scope.filesVar.append('docType', "Telecast_order");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", $scope.filesVar , {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            $window.parent.location.reload();
                           
                      
                        })
                        .error(function (error) {
                            console.log(error);
                        });
    
             
            }else{
                $timeout(function () { 
                    $scope.loading = false;
                    $window.parent.location.reload(); }, 2000);
            }
    
        }
    
   
    $scope.searchInCreatives = function(s) {	
        if(s !== '') {	
             $http.get(apiHostName+"/api/v1/creatives/"+s, {})	
            .then(function (response) {	
            let res = response.data;	
            $scope.creativeObj = angular.copy(res.payload);	
            $scope.clneCreativeList = $scope.creativeObj;	
        })	
        } else {	
            $scope.getCreativeList();	
        }	
    }
    
    $scope.placementSelected = function(name) { 
        console.log(name, 're na');
        $scope.isPlacementSlctd = true;
        $scope.placement = name;
    }
    	
    $scope.nextTab = function(form) {
      console.log("inside next tab click")
      if( $scope.properties.campaign.startDate !== undefined &&
             $scope.properties.campaign.endDate !== undefined &&
             $scope.properties.campaign.clientReferenceNumber !== undefined &&
             $scope.properties.campaign.advertiserName !== undefined &&
             $scope.properties.campaign.primaryCandidateName !== undefined &&
             $scope.properties.campaign.adType !== undefined && $scope.isPlacementSlctd && $scope.airingChannelsList.length > 0 )
             {
                
                 $scope.tabName = 'tab2';
             }
   
    }

}