/**
 * The controller is a JavaScript function that augments the AngularJS scope and exposes functions that can be used in the custom widget template
 * 
 * Custom widget properties defined on the right can be used as variables in a controller with $scope.properties
 * To use AngularJS standard services, you must declare them in the main function arguments.
 * 
 * You can leave the controller empty if you do not need it.
 */
function ($scope,$http, $window,$timeout) {
    var white = 'white';
    $scope.documents = [];
    $scope.istoFileAvailable = false;
    $scope.todocument = {};
    $scope.apiHostName = "http://localhost:9004";
    $scope.isOfficialReceiptSuccess = false;
    $scope.isOfficialError = false;
    $scope.isOfficialNumberError = false;
    
    // add a new variable in AngularJS scope. It'll be usable in the template directly with {{ backgroudColor }} 
    $scope.init = function(){
            //  $http.get($scope.apiHostName+"/api/v1/document?sourceId="+$scope.properties.campaign.campaignId +"&sourceName=Campaign", {})
            //     .then(function (response) {
            //      console.log("response-->",response)
            //         $scope.documents = angular.copy(response.data.payload);
                    
            //         if($scope.documents.length > 0){
            //             for(var i=0;i< $scope.documents.length ; i++){
            //                 var docu = $scope.documents[i];
            //                 if(docu.docType === "Telecast_Order" ){
            //                     $scope.todocument = docu;
            //                 }
            //             }
            //         }
                    
            //     });
    }
    
    $scope.fileOfficialReceiptSelected = function (element) {
            $scope.officialReceipt = element.files[0];
            console.log($scope.officialReceipt);
        }
    
      $scope.submitOfficialRecieptDocuments = function (camp,file,userId) {
           console.log("campaign---",camp);
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                   var doc = file;
                   var formData = new FormData();
                     formData.append('file', doc);
                    formData.append('sourceId', camp.campaignId);
                     formData.append('sourceType', "Campaign");
                    formData.append('userName', userId);
                     formData.append('details',"Treasury");
                     formData.append('docType', "Official_Receipt");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    }).then(function(res){ return res});
            }else{
        }
   
}

$scope.submitORTask = function (camp,userId){ 
    if(camp.payment.officialReceiptNumber === undefined || camp.payment.officialReceiptNumber === '' || camp.payment.officialReceiptNumber === null) {
        $scope.isOfficialNumberError = true;
    } else {
        $scope.isOfficialNumberError = false;
    }
    
    if($scope.officialReceipt === undefined || $scope.officialReceipt === 'undefined' || !$scope.officialReceipt){
        $scope.isOfficialError = true;
    } else {
        $scope.isOfficialError = false;
        $scope.isOfficialReceiptSuccess = true
    }
    
    if(camp.payment.officialReceiptNumber !== undefined && camp.payment.officialReceiptNumber && $scope.officialReceipt !== undefined && $scope.isOfficialReceiptSuccess) {
        var doc = $scope.officialReceipt;
        var formData = new FormData();
        formData.append('file', doc);
        formData.append('sourceId', camp.campaignId);
        formData.append('sourceType', "Campaign");
        formData.append('userName', userId);
        formData.append('details',"Treasury");
        formData.append('docType', "Official_Receipt");
        
        $http.post($scope.apiHostName+"/api/v1/s3/upload", formData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function(res){
            if(res.successful){
                $scope.submitTask(camp);
            }
        })
        .error(function (error) {
            console.log(error);
        });
    }
    
}

 $scope.submitTask = function (camp) {
        var id;
        id = $scope.properties.taskId ;
        
         var data = {
            "campaignInput": camp,
            "userId": $scope.properties.userName
        }
        if (id) {
            $http.put('../API/bpm/humanTask/' + id, { 'assigned_id': $scope.properties.userId }).then(function (response) {
                $http.post('../API/bpm/userTask/' + id + '/execution', data, { 'user': $scope.properties.userId }).then(function (response) {
                }, function (error) {
                });
            }, function (error) {
                //console.log(error);
            });
        } else {
            console.log('Impossible to retrieve the task id value from the URL');
        }
    }
}