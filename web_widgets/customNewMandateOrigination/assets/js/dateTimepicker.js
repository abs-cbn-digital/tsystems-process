angular.module("bonitasoft.ui.widgets").directive('datePickerLink', [ function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {},
        link: function (scope, element, attr, ctrl) {

            var component = element.next();
            if (component.length) {
                component.on('click', function () {
                    element.trigger('focus');
                    //ctrl.$setViewValue(date);
                });
            }
            
        }
    }
}])
    .directive('validDate', [function () {

    return {
        restrict: 'EA',
        link: function (scope, element, attr, ctrl) { 
            ctrl.$validators.validDate = function (viewValue) {
                  var reg = new RegExp(/^(((0[13578]|1[02])\/(0[1-9]|[12]\d|3[01])\/((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\/(0[1-9]|[12]\d|30)\/((19|[2-9]\d)\d{2}))|(02\/(0[1-9]|1\d|2[0-8])\/((19|[2-9]\d)\d{2}))|(02\/29\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g);
                console.log(viewValue);
                if (viewValue != "") { 
                      return reg.test(viewValue);
                 }
                 else
                 {
                      return true;
                 }
            }
        }
    }
}])

    .directive('vistaDatePicker', ['$filter', function ($filter) {

    return {
        restrict: 'EA',
        require: 'ngModel',
        scope: {},
        link: function (scope, element, attr, ctrl) {
            $('.warning-message').hide();
            element.datepicker({
                dateFormat: 'mm/dd/yy',
                onSelect: function (date) {
                    ctrl.$setViewValue(date);
                    element.parent('.input-append').next().children().hide();
                }
            });
            var component = element.next()
            if (component.length) {
                component.on('click', function () {
                    element.trigger('focus');
                    //ctrl.$setViewValue(date);
                });
            }

            element.on('blur', function () {
                var reg = new RegExp(/^(((0[13578]|1[02])\/(0[1-9]|[12]\d|3[01])\/((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\/(0[1-9]|[12]\d|30)\/((19|[2-9]\d)\d{2}))|(02\/(0[1-9]|1\d|2[0-8])\/((19|[2-9]\d)\d{2}))|(02\/29\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g);

                var txt = element.val();
                console.log(txt);
                if (txt != "") {
                    if (reg.test(txt)) {
                        console.log('good');
                        element.parent('.input-append').next().children().hide();
                    } else {
                        element.parent('.input-append').next().children().show();
                    }
                } else {
                    element.parent('.input-append').next().children().hide();
                }
            });
            ctrl.$formatters.unshift(formatDate);

            function formatDate(viewValue) {
                console.log(viewValue);
                var modelVal = ctrl.$modelValue;
                if (modelVal) {
                    var format = $filter('date')(modelVal, 'MM/dd/yyyy');
                    console.log(format);
                    return format;

                }
            }

        }
    }
}]);