angular.module("bonitasoft.ui.widgets").directive('simpleSpinner', ['$timeout', function ($timeout) {
    return {
        restrict: 'EA',
        template: '<i ng-class="class" ng-show="show"></i>',
        scope: {
            show: '=',
            class: '@'
        },
        replace: true,
        link: function (scope, elm, attrs) {
           // scope.showit = scope.show;

            scope.$watch("show", function () {
                if (scope.show) {
    
                    $timeout(function () {
                        //if hasn't change yet
                        if (scope.show) {
                            scope.show = false;
                        }
                    }, 1500);
                } else {
                    scope.show = false;
                }
            });
        }
    };
}
]);