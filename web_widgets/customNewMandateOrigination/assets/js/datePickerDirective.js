angular.module("bonitasoft.ui.widgets").directive('datePicker', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            $(element).datetimepicker({
                format: 'MM/DD/YYYY',
                parseInputDate: function (data) {
                    if (data instanceof Date) {
                        return moment(data);
                    } else {
                        return moment(new Date(data));
                    }
                },
               // maxDate: new Date()
            });

            $(element).on("dp.change", function (e) {
                ngModel.$viewValue = moment(e.date).format('MM/DD/YYYY');
                ngModel.$commitViewValue();
            });
        }
    };
});


