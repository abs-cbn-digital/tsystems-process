 /// angular 1 form
function ($scope, $http, $window, $timeout ) {

    $scope.products = [];
    $scope.agencies = [];
    $scope.advertizers = [];
    $scope.primaryCandidates = [];
    $scope.adTypes = [];
    $scope.airingChannels = [];
    $scope.masterDataAiringchannels = {};
    $scope.creativeList = [];
    $scope.campaignCreatives = [];
     $scope.creative={
         creativeId:''
     }
     
        $scope.campaign={}
   
        $scope.airingChannelsList = [];
     $scope.zIndex = 0;
      $scope.zIndexP = 0;
    var apiHostName = "http://localhost:9004";
       $scope.creativeLLength = 0;
   //      $("#uid_primaryCandidate").show();
   
      $scope.init = function () {
       
        $http.get(apiHostName+"/api/v1/adTypes", {})
                .then(function (response) {
                    let res = response.data;
                    $scope.adTypes = angular.copy(res.payload);
                    
                });
                
        $http.get(apiHostName+"/api/v1/airingChannels", {})
                .then(function (response) {
                    let res = response.data;
                    $scope.airingChannels = angular.copy(res.payload);
                    $scope.masterDataAiringchannels = JSON.stringify($scope.airingChannels);
                })
                
   }
    $scope.getAgency = function() {
       
         console.log($scope.campaign.agency, '$scope.campaign.agency');
          $http.get(apiHostName+"/api/v1/customerByName?accessType=Agency&name="+$scope.campaign.agency, {})
                .then(function (res) {
                    console.log( 'Agency', res);
                    response = res.data;
                    // console.log(response.body.payload, 'Agency');
                    $scope.agencies = angular.copy(response.payload);
                    
                });
    };
    
    $scope.onSelectAgency = function(agencyname){
      
       $scope.campaign.agency = '';
       $scope.campaign.agency = agencyname.longName;
       $("#uid_agency").hide();
    };
    
    $scope.getAdvertiserNotUsed = function(e) {
       
         console.log($scope.properties.campaign.advertiser, '$scope.properties.campaign.advertiser');
          $http.get(apiHostName+"/api/v1/customerByName?accessType=Advertiser&name="+$scope.campaign.advertiser, {})
                .then(function (res) {
                    console.log( 'Agency', res);
                    response = res.data;
                    // console.log(response.body.payload, 'Agency');
                    $scope.advertizers = angular.copy(response.payload);
                    
                });
   }
   
   $scope.addCreatives = function(){
       $scope.creative={
         creativeId:''
     }
         for (var i = 0; i < $scope.creativeList.length; i++) {
              $scope.creativeList[i] ;
           
                   $scope.campaignCreatives.push( { creativeId: $scope.creativeList[i]});
         }
          $scope.creativeLLength = $scope.creativeList.length
            
   }

$scope.zIndexP = 0;   
 var  typingA = false;
     $scope.getAdvertiser = function(e){
     $scope.zIndex = 0;  
    if(!typingA){
          typingA = true;
          $scope.enFlag = false;
          console.log(typingA);
              $timeout(function(){
                  typingA = false;
                            $scope.advertizers = [];
                         
                            $http.get(apiHostName+"/api/v1/customerByName?accessType=Advertiser&name="+ $scope.properties.campaign.advertiserName, {})
                .then(function (res) {
                   console.log( 'Agency', res);
                    response = res.data;
                      if( res.data){
                                 $scope.zIndex = 1;  
                                 $scope.advertizers = angular.copy(response.payload);
                                  if( $scope.advertizers.payloadCount == 0){
                                         console.log("No primaryCandidates ");
                                      $scope.zIndex = 0;  
                                  }
                      }else{
                               $scope.properties.campaign.advertiserName = "";
                            }
              },function(error){
                                  $scope.zIndex = 0;  
                        });
              },1000)
              }
}
   $scope.onSelectAdvertiser = function(advname){
        console.log(advname, 'advname');
          $scope.properties.campaign.advertiserName = '';
          $scope.properties.campaign.advertiserName = advname.longName;
        $("#uid_advertisers").hide();
   }
   
   
    $scope.zIndexP = 0;   
 var  typingB = false;
 
 
  $scope.getPrimaryCandidates = function(e){
     $scope.zIndexP = 0;  
    if(!typingB){
          typingB = true;
          $scope.enFlagNew = false;
          console.log(typingB);
              $timeout(function(){
                  typingB = false;
                            $scope.primaryCandidates = [];
                         
                            $http.get(apiHostName+"/api/v1/candidates/"+$scope.properties.campaign.primaryCandidateName, {})
                .then(function (res) {
                    console.log( 'candidates', res);
                    response = res.data;
                      if( res.data){
                                 $scope.zIndexP = 1;  
                                 $scope.primaryCandidates = angular.copy(response.payload);
                                  if( $scope.primaryCandidates.payloadCount == 0){
                                         console.log("No primaryCandidates ");
                                      $scope.zIndexP = 0;  
                                  }
                      }else{
                               $scope.properties.campaign.primaryCandidateName = "";
                            }
              },function(error){
                                  $scope.zIndexP = 0;  
                        });
              },1000)
              }
}
     
   $scope.onSelectPrimaryCandidate = function(primaryCandidate){
        console.log(primaryCandidate, 'primaryCandidate');
        $scope.properties.campaign.primaryCandidateName = '';
        $scope.properties.campaign.primaryCandidateName= primaryCandidate.candidateName;
        $("#uid_primaryCandidate").hide();
   }
   
   
   
   $scope.airingChannelFilter = function(sletters) {
       let channels = JSON.stringify(JSON.parse($scope.masterDataAiringchannels));
       if(sletters === 'all') {
           filtered = JSON.parse(channels)
       } else {
           letters = sletters.split(',')
        var filtered = [];
        items = JSON.parse(channels)
        console.log(items, 'items', letters)
        for (var l = 0; l < letters.length; l++) {
        var letterMatch = new RegExp(letters[l], 'i');
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              if (letterMatch.test(item.channelName.substring(0, 1))) {
                filtered.push(item);
              }
            }
        }
       }
        $scope.airingChannels = filtered;
    };
   $scope.onSelectImg = function(cid) { 
       
         if(Object.keys($scope.creativeList).length === 0) {
              $scope.creativeList.push(cid);
         } else {
            let isExi = $scope.creativeList.findIndex(c => c === cid);
            if(isExi == -1){
                 $scope.creativeList.push(cid);
            } else {
                 $scope.creativeList.splice(isExi, 1);
            }
         }
       }
       
          $scope.onSelectAiringChannel = function(name) { 
       
         if(Object.keys($scope.airingChannelsList).length === 0) {
              $scope.airingChannelsList.push(name);
         } else {
            let isExi = $scope.airingChannelsList.findIndex(c => c === name);
            if(isExi == -1){
                 $scope.airingChannelsList.push(name);
            } else {
                 $scope.airingChannelsList.splice(isExi, 1);
            }
         }
         
          console.log($scope.airingChannelsList, 'airingChannelsList');
       }

   
     $scope.reloadDashBoard = function () {
        $window.parent.location.reload();
    }
    var URL = "/bonita/portal/resource/process/Campaign Process/1.0/API/bpm/process/" + $scope.properties.processId + "/instantiation"
     
 $scope.saveCampaign = function (campaign,userID) {
         $scope.properties.campaign.agency =userID ;
         var airingChannel =""
          for (var i = 0; i < $scope.airingChannelsList.length; i++) {
              if(airingChannel === ""){
                 airingChannel =  $scope.airingChannelsList[i] ;
              }else{
                      airingChannel = airingChannel+","+$scope.airingChannelsList[i];
              }
         
          }
       campaign.agency = $scope.properties.userId;
       campaign.airingChannel = airingChannel;
       campaign.creatives = $scope.campaignCreatives
       console.log("Final campaign ::", campaign);
        var request = {
            "campaignInput": campaign,
        }
        console.log(request);
        //Rest call for create mandate
        $http.post(URL, request)
            .then(function (response) {
                $scope.submitDocuments(response.data.caseId);
            }, function (error) {
                console.log(error);
            });
    }
    

   
       $scope.submitDocuments = function (caseId) {
           console.log("caseId---",caseId);
               console.log("$scope.files ---",$scope.files )
                 console.log("$scope.files.length ---",$scope.files.length )
    
            if (caseId && $scope.files ) {
                 var documentsLength = $scope.files.length;
                var count = 0; 
          
                    
                    var doc = $scope.files;
                    var formData = new FormData();
                    formData.append('file', doc);
                    formData.append('sourceId', caseId);
                    formData.append('sourceType', "Campaign");
                    formData.append('userName', $scope.properties.userId);
                    formData.append('details', doc.size);
                    formData.append('docType', doc.type);
                   
                    $http.post(apiHostName+"/api/v1/s3/upload",formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            $window.parent.location.reload();
                           
                      
                        })
                        .error(function (error) {
                            console.log(error);
                        });
    
             
            }else{
                $timeout(function () { 
                    $scope.loading = false;
                    $window.parent.location.reload(); }, 2000);
            }
    
        }
    
   
   $scope.creativeObj = [{
    "persistenceId": 1,
    "persistenceId_string": "1",
    "persistenceVersion": 0,
    "persistenceVersion_string": "0",
    "creativeId": 111,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "28 DC",
    "adExecutionType": "Digital - Banner",
    "adMaterialType": "HD",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "Walter",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
    "creativeUrl": "https://images.unsplash.com/photo-1593642532009-6ba71e22f468?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1769&q=80"
    },
    {
    "persistenceId": 2,
    "persistenceId_string": "2",
    "persistenceVersion": 0,
    "persistenceVersion_string": "0",
    "creativeId": 112,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "29 DC",
    "adExecutionType": "2 - Banner",
    "adMaterialType": "HD",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "Walter",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
     "creativeUrl": "https://images.unsplash.com/photo-1593642532009-6ba71e22f468?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1769&q=80"
    },
      {
    "persistenceId": 2,
    "persistenceId_string": "2",
    "persistenceVersion": 0,
    "persistenceVersion_string": "0",
    "creativeId": 113,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "29 DC",
    "adExecutionType": "2 - Banner",
    "adMaterialType": "HD",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "Walter1",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
     "creativeUrl": "https://images.unsplash.com/photo-1593642532009-6ba71e22f468?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1769&q=80"
    },
      {
    "persistenceId": 2,
    "persistenceId_string": "2",
    "persistenceVersion": 0,
    "persistenceVersion_string": "0",
    "creativeId": 114,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "29 DC",
    "adExecutionType":"Digital - Video",
    "adMaterialType": "HD",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "Walter2",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
     "creativeUrl": "http://localhost:9004/api/v1/s3/download?key=abs.PNG&caseId=12344"
    },
      {
    "persistenceId": 2,
    "persistenceId_string": "2",
    "persistenceVersion": 0,
    "persistenceVersion_string": "0",
    "creativeId": 115,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "29 DC",
    "adExecutionType": "2 - Banner",
    "adMaterialType": "HD",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "Walter3",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
     "creativeUrl": "https://images.unsplash.com/photo-1593642532009-6ba71e22f468?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1769&q=80"
    },
    
    {
    "persistenceId": 3,
    "persistenceId_string": "3",
    "persistenceVersion": 0,
    "persistenceVersion_string": "0",
    "creativeId": 116,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "2789 DC",
    "adExecutionType": "23 - Banner",
    "adMaterialType": "HD4",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "sony",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
     "creativeUrl": "https://images.unsplash.com/photo-1593642532009-6ba71e22f468?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1769&q=80"
    },
    {
    "persistenceId": 4,
    "persistenceId_string": "4",
    "persistenceVersion": 4,
    "persistenceVersion_string": "0",
    "creativeId": 117,
    "advertiserName": "1 DIGITAL MEDIA GROUP INC.",
    "agencyName": "32 DC",
    "adExecutionType": "8 - Banner",
    "adMaterialType": "HD7",
    "materialLength": "3:30",
    "materialVersion": "1",
    "primaryCandidate": "bates",
    "candidateType": "test",
    "dialect": true,
    "adPaidBy": "Sonny Angara",
    "adPaidFor": "Panfilo Lacson",
    "status": "Approved",
    "aEInternalStatus": "Approved",
    "stratApprovalStatus": "Approved",
    "updatedBy": null,
    "updatedOn": null,
    "createdBy": "walter.bates",
    "createdOn": "2021-10-26T17:20:53.8393243Z",
    "creativeUrl": "https://images.unsplash.com/photo-1593642532009-6ba71e22f468?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1769&q=80"
    }];

}