angular.module("bonitasoft.ui.widgets").directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                console.log(scope.files,'begining');
                console.log(element[0].files, 'elements');
                if(scope.files && scope.files.length > 0){
              
                         angular.forEach(scope.files, function (item) { 
                                 values.push(item);
                        });
                }
                 console.log(element[0].files, 'elements after');
                angular.forEach(element[0].files, function (item) {
                    values.push(item);
                });
                scope.$apply(function () {
                    console.log(values, 'apply');
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);