/**
 * The controller is a JavaScript function that augments the AngularJS scope and exposes functions that can be used in the custom widget template
 * 
 * Custom widget properties defined on the right can be used as variables in a controller with $scope.properties
 * To use AngularJS standard services, you must declare them in the main function arguments.
 * 
 * You can leave the controller empty if you do not need it.
 */
function ($scope, $http, $window, $timeout,toaster) {
    
    //console.log("In");
    //console.log($scope.properties.mandateData);
    
    // $scope.getComments = function(role){
    //       $http.post("/bonita/API/extension/smekpi?f=getcomments&c="+$scope.properties.mandateData.mandateId+"&p=NO&t="+role,{})
    //           .then(function (response) {
    //             console.log(response.body,"comments");           
    //         });
    // }
     $scope.zIndexC = 0;
    $scope.zIndexE = 0;  
      $scope.enableComments = false;
    $scope.enable = function(){
         $scope.enableComments = true;
    }
     $scope.zIndex = 0;
    $scope.inits = function(files,index){
        console.log("in");
        console.log(files.length,"****",index);
        var n = files.length;
        
            if(index + 1 == n){
                console.log("i reached");
                 $scope.pop("Documents uploaded")
            }
        
       
    }
    
      $scope.tue= function(s){
    //console.log(s,"Date");
}
    $scope.zIndex = 0;
      $scope.commentsObj;
        $scope.opacity;
        $scope.loading; 
     $scope.selectedActivities = [];
     $scope.typing = false;
    $scope.nameOfProductId = '';
    $scope.creditExpo = [];
    $scope.baVendor = [{}];
   
    $scope.showbaVendor = false;
     $scope.enFlag = false;
    $scope.ismeDropDown;
    $scope.isIndv =false;
    $scope.noFlag =false;
    $scope.baCompanyList = [];
    $scope.baContactList = [];
    $scope.baActivities = []; 
 // $scope.baActivities = [{"ID":1,"ACTIVITY":"Not applicable"},{"ID":2,"ACTIVITY":"Only financials"},{"ID":3,"ACTIVITY":"Only Bank statement"},{"ID":4,"ACTIVITY":"Audit/Projects"},{"ID":5,"ACTIVITY":"Credit check"},{"ID":6,"ACTIVITY":"Assessment"},{"ID":7,"ACTIVITY":"Fact sheet and financials"},{"ID":8,"ACTIVITY":"Full Case"},{"ID":9,"ACTIVITY":"Legal Due Diligence"},{"ID":10,"ACTIVITY":"ROC"},{"ID":11,"ACTIVITY":"Site visit only"},{"ID":12,"ACTIVITY":"Profitability Analysis"},{"ID":13,"ACTIVITY":"Vendor Registration & KYC"},{"ID":14,"ACTIVITY":"Verification"},{"ID":15,"ACTIVITY":"Site visit+document collection"},{"ID":16,"ACTIVITY":"Portal verification"},{"ID":17,"ACTIVITY":"Info collection and submission"}]
    $scope.actionDocuments = [];
    $scope.uploadListActionDocuments = [];
    $scope.baActivitiesModel = [];
    $scope.vendorActivityName = [];
    $scope.vendorCompanyName = [];
    $scope.vendorContactName = [];
    $scope.uploadedList = [];
    $scope.smeBa = [];
    // $scope.smeBa = [{
    //   	        name: '',
    //   	        contactName: '',
    //   	        smeBAActivity: ''
    //   	    }];
$scope.smeBaItem = {
        name: '',
        contactName: '',
        windowsId: '',
        smeBAActivity: '',
        smeBAScheduledDate: ''
    };
     $scope.datePick =[];

    $scope.baseUrl = "http://dev-bonita.smefirst.com:8080/"
  $scope.gstNotMatch = false; 
  
  $scope.getGSTSEZvalue = function(data){
      //console.log(" datais_sez_contact_flag---", data.is_sez_contact_flag,"==properties.mandateData.sez===",$scope.properties.mandateData.sez);
     if(data.is_sez_contact_flag == 'Y'  && $scope.properties.mandateData.sez == 'Y'){
       
          $scope.gstNotMatch = false;
     }else if(data.is_sez_contact_flag == 'N'  && $scope.properties.mandateData.sez == 'N'){
          $scope.gstNotMatch = false;
     }else{
         $scope.properties.mandateData.gstNumber = "";
         $scope.gstNotMatch = true;
     }
  }
      

  $scope.caseSubstatusPopup = function(){
      
      if($scope.properties.mandateData.ismeCaseSubStatus == ''){
          //console.log("empty");
          
            $('#functionUsersCheck').modal('show');
          
      }else{
          //console.log("not empty");
          saveForlater();
      }
       
        
    
   }



            //getting grading fees 
    $scope.changeExposure = function(name){
        ////console.log( 'selec', '-----',name['ID']);
        //console.log( 'productid', '-----',$scope.nameOfProductId);
        //console.log(name,"name obj");
   // $scope.properties.mandateData.productName = vName;
	//$scope.properties.mandateData.institutionName = vName; 
                $http.post("/bonita/API/extension/getMasterData?f=getGradingFee",{ "producttypename": $scope.properties.mandateData.productType, "productcategoryname": $scope.properties.mandateData.productCategory,"credit_exposure" : $scope.properties.mandateData.creditExposureByBank,"productname":$scope.properties.mandateData.productName,"institutionname":$scope.properties.mandateData.institutionName })
                    .then(function (result) {
                        //console.log(result, '<--result fee-->', result.data);
                    if(result.data[0]){
                        $scope.properties.mandateData.gradingFees = result.data[0].RATINGFEE
                       }else{
                    //   $scope.properties.mandateData.gradingFees = 0;  
                    }
                    }, function (error) {
                        //console.log(error);
                });
        
    }
  var  typing = false;
    //Add Vendor Method
$scope.getEntities = function(e){
    $scope.zIndex = 0;
if(!typing){
      typing = true;
       $scope.enFlag = false;
      //console.log(typing);
          $timeout(function(){
              typing = false;
                //console.log($scope.properties.mandateData.entityName);
              
                        $scope.baCompanies = [];
                        //console.log("in");
                        $http.post("/bonita/API/extension/getMasterData?f=bacompaniesbycontact",{"companyName":$scope.properties.mandateData.entityName,"contactName":$scope.properties.mandateData.billingCompanyContact})
                        .then(function(response){
                        //console.log("In bacompany");
                        if(response.data){
                                $scope.zIndex = 1;
                        $scope.entites = angular.copy(response.data);
                        if($scope.entites.length == 0){
                                $scope.zIndex = 0;
                        }
                        }else{
          $scope.properties.mandateData.entityName="";
       $scope.properties.mandateData.entityAddress="";
        $scope.properties.mandateData.entityContactPersonNumber="";
         $scope.properties.mandateData.entityDisplayName="";
          $scope.properties.mandateData.companyContactId ="";
           $scope.properties.mandateData.companyCode="";
                        }
                        //console.log($scope.entites);
                        },function(error){
                        //console.log(err);
                    });
              
          },1000)
          }
}

//Entity Name drop-down select

$scope.fillTextbox =function(output){
      //console.log(output.company_name,"fil box");
       $scope.zIndex = 0;
      $scope.enFlag = true;
      $scope.properties.mandateData.entityName=output.company_name;
       $scope.properties.mandateData.entityAddress=output.address1;
        $scope.properties.mandateData.entityContactPersonNumber=output.phone_num;
         $scope.properties.mandateData.entityDisplayName=output.display_company_name;
         $scope.properties.mandateData.companyCode = output.company_code;
          if(output.company_code && $scope.properties.mandateData.billingCompanyContact){
                 $http.post("/bonita/API/extension/getMasterData?f=getBACompanyContact",{"companyCode":output.company_code,"contact":$scope.properties.mandateData.billingCompanyContact})
                        .then(function(response1){
                        //console.log("In getBACompanyContact");
                        if(response1.data[0]){
                       // $scope.entites = angular.copy(response.data);
                         $scope.properties.mandateData.companyContactId = response1.data[0].contact_id;
                        }
                        },function(error){
                        //console.log(err);
                    });
                }
      $scope.baCompanies=null;
       
      
}


//delete document

$scope.deleteDocument = function (responsecode,index) {
 //console.log(index,"index");
 //console.log(responsecode,"responsecode");
 var formData = new FormData();
 formData.append('responseCode', responsecode);
 formData.append('mandateId',  $scope.properties.mandateData.mandateId);
 // formData.append('s3Path', "NO_DOCUMENT");

 $http.post("/bonitaAPI/accesssme/deleteDocument", formData, {
     transformRequest: angular.identity,
     headers: { 'Content-Type': undefined }
 })
     .success(function (response) {
         //console.log(response);
         $scope.getUpdatedDocs();
         	$scope.pop("Document deleted");
        // //console.log(response.data);
        // //console.log(response.response);
        // $scope.properties.mandate.scannedMandate += response.response + ","
     })
     .error(function (error) {
         //console.log(error);
         $scope.getUpdatedDocs();
     });


};


//get uodated uploaded list

$scope.getUpdatedDocs = function(){
   $http.post("/bonita/API/extension/getMasterData?f=getuploadedfile", { "mandateId": $scope.properties.mandateId })
            .then(function (response) {
                //console.log("Upload List ************");
                //console.log(response.data);

                $scope.uploadedList = angular.copy(response.data);
            });
}


    $scope.addVendor = function () {
        //console.log("IN Ba vendor");
        

        $scope.baVendor.push({});
         $scope.datePick.push({'statu':false});
        $scope.smeBa.push(Object.assign({}, $scope.smeBaItem));
        
   
    }

 
    $scope.dleteVendor = function (i) {
     
        
    if($scope.baVendor && $scope.baVendor.length > 1){
        $scope.baVendor.splice(i, 1);
          $scope.datePick.splice(i, 1);
        $scope.smeBa.splice(i, 1);
        
    }else{
         $scope.pop("Atleast one ba is required")
    }

    }
    
    $scope.allocatedToCommittee = function (e) {
        //console.log(e.target.value);
        $scope.properties.mandateData.allocateToCommitteeDate = e.target.value;


        //console.log($scope.properties.mandateData.allocateToCommitteeDate, "Allocated commitee date");

    }

    $scope.paymentDate = function (e) {
        //console.log(e.target.value);
        $scope.properties.mandateData.paymentDate = e.target.value; 
        //console.log($scope.properties.mandateData.paymentDate);

    }







    //  $scope.deleteOne = function(documentIndex){
    //      //console.log(documentIndex);
    //       $scope.files.splice(documentIndex,1);
    //  } 


    $scope.deleteUploadDoc = function (i) {

        $scope.files.splice(i, 1);
         $scope.pop("Document deleted")

    }


    $scope.deleteOne = function (doc, i) {

        var document = {
            "mandateId": doc.mandate_id,
            "responseCode": doc.response_code
        }

        //console.log(document);

        $http.post("/bonita/API/extension/getMasterData?f=updatefiledeletestatus", document)
            .success(function (response) {
                //console.log(response);
                $scope.uploadedList.splice(i, 1);
                 $scope.pop("Document deleted")
            })
    }


    $scope.submitDocuments = function (mandateId) {

        if (mandateId && $scope.files && $scope.files.length > 0) {
            var documentsLength = $scope.files.length;
            var count = 0;
            for (var i = 0; i < $scope.files.length; i++) {
              
                var doc = $scope.files[i];
                var formData = new FormData();
                formData.append('file', doc);
                formData.append('mandateId', mandateId);
                formData.append('s3Path', "NO_DOCUMENT");

                $http.post("/bonitaAPI/accesssme/uploadFile", formData, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
                    .success(function (response) {
                        //console.log(response);
                        count = count + 1;
                      //  $scope.getUpdatedDocs();
                        if(count >= documentsLength){
                            //console.log("uploaded docs successfully..!!!!");
                            $timeout(function () { 
                                $scope.loading = false;
                                $window.parent.location.reload(); }, 2000);
                          }
                    })
                    .error(function (error) {
                        //console.log(error);
                       // $scope.getUpdatedDocs();
                    });

            }
        }else{
            $timeout(function () { 
                $scope.loading = false;
                $window.parent.location.reload(); }, 2000);
        }

    }

    $scope.documentAction = function (document, status) {

        //console.log(status);
        //console.log(document);
        if (status == true) {
            $scope.actionDocuments.push(document);
        } else {

            //console.log("in splice");
            var index = $scope.actionDocuments.indexOf(document);
            $scope.actionDocuments.splice(index, 1);

        }

        //console.log($scope.actionDocuments, 'uploaded documents');


    }

    $scope.documentActionForUploadList = function (document, status) {
        
                //console.log(status);
                //console.log(document);
                if (status == true) {
                    $scope.uploadListActionDocuments.push(document);
                } else {
        
                    //console.log("in splice");
                    var index = $scope.uploadListActionDocuments.indexOf(document);
                    $scope.uploadListActionDocuments.splice(index, 1);
        
                }
        
                //console.log($scope.uploadListActionDocuments, 'uploaded documents');
        
        
            }

    $scope.delete = function () {
            //deleting local docs
        angular.forEach($scope.actionDocuments, function (value, key) {
            //console.log(value);
            //console.log($scope.files[value]);
            //  delete $scope.files[value];
            $scope.files.splice(value, 1);

        })
     
          //deleting server docs
          if($scope.uploadListActionDocuments.length > 0){
        angular.forEach($scope.uploadListActionDocuments, function (value, key) {
        
            //console.log(value,"response code")
        
                var formData = new FormData();
                formData.append('responseCode', value);
                formData.append('mandateId',  $scope.properties.mandateData.mandateId);
                // formData.append('s3Path', "NO_DOCUMENT");

                $http.post("/bonitaAPI/accesssme/deleteDocument", formData, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
                    .success(function (response) {
                        //console.log(response);
                        //console.log(response.data);
                        
                        $scope.getUpdatedDocs();
                         // $scope.pop("Documents deleted")
                             $scope.pop("Documents deleted")
                       // //console.log(response.response);
                       // $scope.properties.mandate.scannedMandate += response.response + ","
                    })
                    .error(function (error) {
                        //console.log(error);
                        $scope.getUpdatedDocs();
                    });

            
        })
        }else{
           $scope.pop("Documents deleted");  
        }
        $scope.actionDocuments = [];
        $scope.uploadListActionDocuments = [];
        //console.log($scope.files);
        //console.log($scope.actionDocuments,"acrtion docs")

    }

    $scope.deleteAll = function () {
        //deleting local docs
        $scope.files = [];
        
        //deleting uploaded docs
        if($scope.uploadedList.length>0){
        angular.forEach($scope.uploadedList, function (value, key) {
            
                //console.log(value.response_code,"response code")
            
                    var formData = new FormData();
                    formData.append('responseCode', value.response_code);
                    formData.append('mandateId',  $scope.properties.mandateData.mandateId);
                    // formData.append('s3Path', "NO_DOCUMENT");

                    $http.post("/bonitaAPI/accesssme/deleteDocument", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            //console.log(response);
                          //  //console.log(response.data);
                            $scope.getUpdatedDocs();
                             $scope.pop("Documents deleted");
                           // //console.log(response.response);
                           // $scope.properties.mandate.scannedMandate += response.response + ","
                        })
                        .error(function (error) {
                            //console.log(error);
                            $scope.getUpdatedDocs();
                        });

                
            })
        }
        else{
           $scope.pop("Documents deleted"); 
        }
    }

 
 $scope.bdHistory = []

//changed By GOPAL
$scope.products = [];
$scope.institutions = [];
$scope.billingCompanies = [];
$scope.billingCompanyContacts = [];


//changed By GOPAL
$scope.getProduct = function (vName) {
    $scope.properties.mandateData.productName = vName;
	
}
$scope.getInstitution = function (vName) {
     $scope.properties.mandateData.institutionName = vName; 
    
}


var  typingB = false;
$scope.getBillingCompany = function(e){
 $scope.zIndex = 0;
if(!typingB){
      typingB = true;
      $scope.enFlagNew = false;
      //console.log(typingB);
          $timeout(function(){
              typingB = false;
                //console.log($scope.properties.mandateData.billingCompany);
               $scope.properties.mandateData.billingCompanyContact = "";
                $scope.gstApplicable = true;  
                      $scope.properties.mandateData.companyCode ="";
                       $scope.properties.mandateData.gstNumber ="";
                        $scope.properties.mandateData.gstExempt ="";
                    $scope.properties.mandateData.sez ="";
                    $scope.properties.mandateData.gstNumberNotApplicable ="";
                        $scope.billingCompanies = [];
                        //console.log("in");
                        $http.post("/bonita/API/extension/getMasterData?f=bacompany",{"companyName":$scope.properties.mandateData.billingCompany})
                        .then(function(response){
                               //console.log("In bacompany");
                        if(response.data){
                            
                               $scope.billingCompanies = angular.copy(response.data);
                                $scope.zIndex = 1;
                               if($scope.billingCompanies.length == 0){
                                    $scope.zIndex = 0;
                               }
                        }else{
                              $scope.zIndex = 0;
                          $scope.properties.mandateData.billingCompany="";
                          $scope.properties.mandateData.companyCode="";
                           $scope.properties.mandateData.billingCompanyContact = "";
                        }
                        //console.log($scope.billingCompanies);
                        },function(error){
                        //console.log(err);
                    });
              
          },1000)
          }
          
           
     if($scope.properties.mandateData.institutionName == "Individual"){
    $scope.isIndv = true;
    
     $http.post("/bonita/API/extension/getMasterData?f=entityname", { "entityName": $scope.properties.mandateData.billingCompany,"contactName":$scope.properties.mandateData.billingCompanyContact})
            .then(function (response) {
                //console.log("!!!!!!!!!!!!!!!!!" + "********");
                //console.log(response.data);
                $scope.properties.mandateData.entityName = response.data.company_name;
                $scope.properties.mandateData.entityAddress = response.data.address1;
                $scope.properties.mandateData.entityContactPersonNumber = response.data.phone_num;
                //$scope.properties.mandate.companyCode = response.data.company_code;
                $scope.properties.mandateData.entityEmailForCommunication = response.data.email;
                $scope.properties.mandateData.entityCity = response.data.city;
                $scope.properties.mandateData.entityState = response.data.state;
                
               // $scope.properties.mandate.companyContactId = "CRISILHOUSE01/ContactID/37879/NAST-5XWUL4";
                $scope.properties.mandateData.entityDisplayName = response.data.display_company_name;
                
                 if($scope.properties.mandateData.companyCode && $scope.properties.mandateData.billingCompanyContact){
                 $http.post("/bonita/API/extension/getMasterData?f=getBACompanyContact",{"companyCode":$scope.properties.mandateData.companyCode,"contact":$scope.properties.mandateData.billingCompanyContact})
                        .then(function(response1){
                        //console.log("In getBACompanyContact");
                        if(response1.data[0]){
                       // $scope.entites = angular.copy(response.data);
                         $scope.properties.mandateData.companyContactId = response1.data[0].contact_id;
                        }
                        },function(error){
                        //console.log(err);
                    });
                }
           
            }, function (error) {
                //console.log(err);
            });
}else{
     $scope.isIndv = false; 
     
       $scope.properties.mandateData.entityName = "";
                $scope.properties.mandateData.entityAddress= "";
                $scope.properties.mandateData.entityContactPersonNumber = "";
                //$scope.properties.mandate.companyCode = response.data.company_code;
                $scope.properties.mandateData.entityEmailForCommunication = "";
                $scope.properties.mandateData.entityCity = "";
                $scope.properties.mandateData.entityState = "";
                
               // $scope.properties.mandate.companyContactId = "CRISILHOUSE01/ContactID/37879/NAST-5XWUL4";
                $scope.properties.mandateData.entityDisplayName = "";
}
}

$scope.getBillingCompanyBox =function(output){
      //console.log(output.company_name,"fil box");
      $scope.enFlagNew = true;
       $scope.zIndex = 0;
      $scope.properties.mandateData.billingCompany=output.company_name;
     $scope.properties.mandateData.companyCode = output.company_code;
      $scope.properties.mandateData.state = output.state;
      
          if(output.company_code){
                 $http.post("/bonita/API/extension/getMasterData?f=bacompaniescontact", { "companyName": $scope.properties.mandateData.billingCompany })
            .then(function (response) {
              //  $scope.billingCompanyContacts = angular.copy(response.data);
                
            }, function (error) {
                //console.log(err);
            });
                }
      $scope.billingCompanies=[];
}
 $scope.getBillingCompanyContact = function (vName,bName) {
      $scope.properties.mandateData.billingCompanyContact = vName;
       $scope.zIndexC = 0;
      //GST :Logic
        $scope.enFlagNew1 = true;
       var feeMaster = {
          
            "contact_full_name": $scope.properties.mandateData.billingCompanyContact,
            "company_name": $scope.properties.mandateData.billingCompany
        }
        //console.log(feeMaster);
        $http.post("/bonita/API/extension/getMasterData?f=feeMaster", feeMaster)
            .then(function (response) {
                //console.log("$$$$$$$$$");
                //console.log(response.data);
                //console.log("$$$$$$$$$");
                ////console.log(response.data[0].GST_NUMBER);
                $scope.properties.mandateData.gstNumber = response.data[0].GST_NUMBER;
                $scope.properties.mandateData.locationOfService = response.data[0].LOCATION;
                $scope.properties.mandateData.gstExempt = response.data[0].GST_EXEMPT;
                $scope.properties.mandateData.sez = response.data[0].IS_SEZ_GST_FLAG;
                $scope.properties.mandateData.gstNumberNotApplicable = response.data[0].GST_NUMBER_NOT_APPLICABLE;
         
             //    $scope.properties.mandateData.gradingFees = 0
                 
                  if( $scope.properties.mandateData.gstExempt =='N' &&  $scope.properties.mandateData.gstNumberNotApplicable == 'N'){
                   // $scope.properties.mandate.gstNumber = 'NA'   ;
                  $scope.gstApplicable = false;
                   //console.log("---$scope.properties.mandate.state ---",$scope.properties.mandateData.state );
                   if( $scope.properties.mandateData.billingCompanyContact && $scope.properties.mandateData.billingCompany ){
                       
        $http.post("/bonita/API/extension/getMasterData?f=bacompanygst", { "companyName": $scope.properties.mandateData.billingCompany,"contactName":  $scope.properties.mandateData.billingCompanyContact,"gst": response.data[0].IS_SEZ_GST_FLAG})
            .then(function (response) {
                $scope.gstNumbers = angular.copy(response.data);
                //console.log("GST NUMBERS");
                //console.log($scope.gstNumbers);
                //console.log("GST NUMBERS");
                  if($scope.gstNumbers && $scope.gstNumbers.length == 1){
                   $scope.properties.mandateData.gstNumber = $scope.gstNumbers[0].GST_Number ;
                   $scope.gstApplicable = true;  
                }
            }, function (error) {
                //console.log(err);
            });
        }    
            
                }else{
                    $scope.properties.mandateData.gstNumber = 'NA'   ;
                   $scope.gstApplicable = true;
                }
            }, function (error) {
                //console.log(error);
            });
      
       //GST :Logic
       
        
     if($scope.properties.mandateData.institutionName == "Individual"){
    $scope.isIndv = true;
    
     $http.post("/bonita/API/extension/getMasterData?f=entityname", { "entityName": $scope.properties.mandateData.billingCompany ,"contactName":$scope.properties.mandateData.billingCompanyContact})
            .then(function (response) {
                //console.log("!!!!!!!!!!!!!!!!!" + "********");
                //console.log(response.data);
                $scope.properties.mandateData.entityName = response.data.company_name;
                $scope.properties.mandateData.entityAddress = response.data.address1;
                $scope.properties.mandateData.entityContactPersonNumber = response.data.phone_num;
                //$scope.properties.mandate.companyCode = response.data.company_code;
                $scope.properties.mandateData.entityEmailForCommunication = response.data.email;
                $scope.properties.mandateData.entityCity = response.data.city;
                $scope.properties.mandateData.entityState = response.data.state;
                
               // $scope.properties.mandate.companyContactId = "CRISILHOUSE01/ContactID/37879/NAST-5XWUL4";
                $scope.properties.mandateData.entityDisplayName = response.data.display_company_name;
                
                 if($scope.properties.mandateData.companyCode && $scope.properties.mandateData.billingCompanyContact){
                 $http.post("/bonita/API/extension/getMasterData?f=getBACompanyContact",{"companyCode":$scope.properties.mandateData.companyCode,"contact":$scope.properties.mandateData.billingCompanyContact})
                        .then(function(response1){
                        //console.log("In getBACompanyContact");
                        if(response1.data[0]){
                       // $scope.entites = angular.copy(response.data);
                         $scope.properties.mandateData.companyContactId = response1.data[0].contact_id;
                        }
                        },function(error){
                        //console.log(err);
                    });
                }
           
            }, function (error) {
                //console.log(err);
            });
}else{
     $scope.isIndv = false; 
     
       $scope.properties.mandateData.entityName = "";
                $scope.properties.mandateData.entityAddress= "";
                $scope.properties.mandateData.entityContactPersonNumber = "";
                //$scope.properties.mandate.companyCode = response.data.company_code;
                $scope.properties.mandateData.entityEmailForCommunication = "";
                $scope.properties.mandateData.entityCity = "";
                $scope.properties.mandateData.entityState = "";
                
               // $scope.properties.mandate.companyContactId = "CRISILHOUSE01/ContactID/37879/NAST-5XWUL4";
                $scope.properties.mandateData.entityDisplayName = "";
}
      
      
}

    //init method
$scope.init = function () {     

   


   $http.post("/bonita/API/extension/getMasterData?f=products", {})
            .then(function (response) {
                //console.log(response.body);
                $scope.products = angular.copy(response.data);
                //  $scope.baActivitiesModel = angular.copy(response.data);
            });
    $http.post("/bonita/API/extension/getMasterData?f=institutions", {})
            .then(function (response) {
                //console.log(response.body);
                $scope.institutions = angular.copy(response.data);
                //  $scope.baActivitiesModel = angular.copy(response.data);
            });
            
             $http.post("/bonita/API/extension/getMasterData?f=bacompany",{"companyName":$scope.properties.mandateData.billingCompany})
                        .then(function(response){
                               //console.log("In bacompany");
                        if(response.data){
                            //console.log("=response.data[0].company_code----",response.data[0].company_code);
                             $scope.properties.mandateData.companyCode=response.data[0].company_code;   
                        }else{
                            $scope.properties.mandateData.companyCode = null;
                        }
                       // //console.log($scope.billingCompanies);
                        },function(error){
                        //console.log(err);
                    });

     //Rest call for baCompany GST Numbers

        $http.post("/bonita/API/extension/getMasterData?f=bacompaniescontact", { "companyName": $scope.properties.mandateData.billingCompany })
            .then(function (response) {
              //  $scope.billingCompanyContacts = angular.copy(response.data);
                
            }, function (error) {
                //console.log(err);
            });





  //changed By GOPAL

// $scope.properties.mandate.locationOfService = "Maharashtra";
if($scope.properties.netopsButtonData == "NO"){
    $scope.noFlag = true;
}

 if($scope.properties.mandateData.institutionName == "Individual"){
    $scope.isIndv = true;
    //console.log('$scope.properties.mandateData.entityName::::',$scope.properties.mandateData.entityName);
     $http.post("/bonita/API/extension/getMasterData?f=entityname", { "entityName": $scope.properties.mandateData.billingCompany ,"contactName":$scope.properties.mandateData.billingCompanyContact})
            .then(function (response) {
                //console.log("!!!!!!!!!!!!!!!!!" + "********");
                //console.log(response.data);
                $scope.properties.mandateData.entityName = response.data.company_name;
                $scope.properties.mandateData.entityAddress = response.data.address1;
                $scope.properties.mandateData.entityContactPersonNumber = response.data.phone_num;
                 $scope.properties.mandateData.companyCode = response.data.company_code;
                     $scope.properties.mandateData.entityDisplayName = response.data.display_company_name;
                     //   $scope.properties.mandateData.companyCode = response.data.company_code;
                 if(response.data.company_code && $scope.properties.mandateData.billingCompanyContact){
                 $http.post("/bonita/API/extension/getMasterData?f=getBACompanyContact",{"companyCode":response.data.company_code,"contact":$scope.properties.mandateData.billingCompanyContact})
                        .then(function(response1){
                        //console.log("In getBACompanyContact");
                        if(response1.data[0]){
                       // $scope.entites = angular.copy(response.data);
                        // $scope.properties.mandateData.companyContactId = response1.data[0].contact_id;
                        }
                        },function(error){
                        //console.log(err);
                    });
                }
                  
                

            }, function (error) {
                //console.log(err);
            });
}


    // Rest call for credit-Exposure by Bank
      $http.post("/bonita/API/extension/getMasterData?f=creditexposure",{})
      .then(function(response){
         //console.log("********************************");
         $scope.creditExpo = response;
         //console.log("credit exposure", response);
      },function(error){
          //console.log(error);
      });

        //Rest call for ISME Drop down

        $http.post("/bonita/API/extension/getMasterData?f=ismesubstatus", {})
            .then(function (response) {
                //console.log("ISME DROp down");
                //console.log(response.data);
                $scope.ismeDropDown = angular.copy(response.data);
            })
  $http.post("/bonita/API/extension/getMasterData?f=bacompanygst", { "companyName": $scope.properties.mandateData.billingCompany,"contactName":  $scope.properties.mandateData.billingCompanyContact,"gst": $scope.properties.mandateData.sez})
            .then(function (response) {
                $scope.gstNumbers = angular.copy(response.data);
                //console.log("GST NUMBERS");
                //console.log($scope.gstNumbers);
                //console.log("GST NUMBERS");
                  if($scope.gstNumbers && $scope.gstNumbers.length == 1){
                       $scope.properties.mandateData.gstNumber = $scope.gstNumbers[0].GST_Number ;
                       $scope.gstApplicable = true;  
                    }
            }, function (error) {
                //console.log(err);
            });
            //removed gradding fee
//  $http.post("/bonita/API/extension/getMasterData?f=getGradingFee",{ "producttypename": $scope.properties.mandateData.productType, "productcategoryname": $scope.properties.mandateData.productCategory,"credit_exposure" : $scope.properties.mandateData.creditExposureByBank,"productname":$scope.properties.mandateData.productName,"institutionname":$scope.properties.mandateData.institutionName })
//                     .then(function (result) {
//                         //console.log(result, '<--result fee-->', result.data);
//                     if(result.data[0]){
//                         $scope.properties.mandateData.gradingFees = result.data[0].RATINGFEE
//                     }else{
//                     //  $scope.properties.mandateData.gradingFees = 0;  
//                     }
//                     }, function (error) {
//                         //console.log(error);
//                 });
     
      

        //Rest call for PO Master
        $http.post("/bonita/API/extension/getMasterData?f=po", { "instituteName": $scope.properties.mandateData.instituteName })
            .then(function (response) {
                //console.log("PO NUMBER");
                $scope.poNumber = angular.copy(response.data);
                //console.log($scope.poNumber);
                //console.log("***************");
            }, function (error) {
                //console.log(err);
            });

     $http.post("/bonita/API/extension/mandateUtility?f=getBDrejectedHistory", { "globalMandateId": $scope.properties.mandateData.globalMandateId})
            .then(function (response) {
                console.log(" bdHistory",response);
                 $scope.bdHistory = angular.copy(response.data);
                
                //console.log($scope.poNumber);
                //console.log("***************");
            }, function (error) {
                //console.log(err);
            });

        //Rest call for getting productTypes

        $http.post("/bonita/API/extension/getMasterData?f=producttypes", {})
            .then(function (response) {
                $scope.productType = angular.copy(response.data);
                //console.log($scope.productType);
                //console.log("***************");
            }, function (error) {
                //console.log(err);
            });

        $http.post("/bonita/API/extension/getMasterData?f=productcategory", {})
            .then(function (response) {
                //console.log("&&&&&&&&&&&&&&&&&");
                $scope.productcategory = angular.copy(response.data);
                //console.log($scope.productcategory);
                //console.log("***************");
            }, function (error) {
                //console.log(err);
            });


        //Rest call for entity drop down

        $http.post("/bonita/API/extension/getMasterData?f=entity", {})
            .then(function (response) {
                $scope.entities = angular.copy(response.data);
                //console.log($scope.entities);
            }, function (error) {
                //console.log(err);
            });
           
             $http.get("/bonitaAPI/accesssme/getBDTeamDetails", {})
                .then(function (response) {
                      console.log("bdNames-response-",response.data);
                    $scope.bdNames = angular.copy(response.data);
                    console.log("bdNames--",$scope.bdNames);
                }, function (error) {
                    console.log(err);
                });

 var feeMaster = {
          
            "contact_full_name": $scope.properties.mandateData.billingCompanyContact,
            "company_name": $scope.properties.mandateData.billingCompany
        }
        //console.log(feeMaster);
        $http.post("/bonita/API/extension/getMasterData?f=feeMaster", feeMaster)
            .then(function (response) {
                //console.log("$$$$$$$$$");
                //console.log(response.data);
                //console.log("$$$$$$$$$");
                ////console.log(response.data[0].GST_NUMBER);
                $scope.properties.mandateData.gstNumber = response.data[0].GST_NUMBER;
                $scope.properties.mandateData.locationOfService = response.data[0].LOCATION;
                $scope.properties.mandateData.gstExempt = response.data[0].GST_EXEMPT;
                $scope.properties.mandateData.sez = response.data[0].IS_SEZ_GST_FLAG;
                $scope.properties.mandateData.gstNumberNotApplicable = response.data[0].GST_NUMBER_NOT_APPLICABLE;
         
                // $scope.properties.mandateData.gradingFees = 0
                 
                  if( $scope.properties.mandateData.gstExempt =='N' &&  $scope.properties.mandateData.gstNumberNotApplicable == 'N'){
                   // $scope.properties.mandate.gstNumber = 'NA'   ;
                  $scope.gstApplicable = false;
                   //console.log("---$scope.properties.mandate.state ---",$scope.properties.mandateData.state );
                   if( $scope.properties.mandateData.billingCompanyContact && $scope.properties.mandateData.billingCompany ){
                       
        $http.post("/bonita/API/extension/getMasterData?f=bacompanygst", { "companyName": $scope.properties.mandateData.billingCompany,"contactName":  $scope.properties.mandateData.billingCompanyContact,"gst": response.data[0].IS_SEZ_GST_FLAG})
            .then(function (response) {
                $scope.gstNumbers = angular.copy(response.data);
                //console.log("GST NUMBERS");
                //console.log($scope.gstNumbers);
                //console.log("GST NUMBERS");
                  if($scope.gstNumbers && $scope.gstNumbers.length == 1){
                   $scope.properties.mandateData.gstNumber = $scope.gstNumbers[0].GST_Number ;
                   $scope.gstApplicable = true;  
                }
            }, function (error) {
                //console.log(err);
            });
        }    
            
                }else{
                    $scope.properties.mandateData.gstNumber = 'NA'   ;
                   $scope.gstApplicable = true;
                }
            }, function (error) {
                //console.log(error);
            });
      
       //GST :Logic
        /// get BA Companies
$scope.baFlag = false;
        $http.post("/bonita/API/extension/getMasterData?f=ba", {})
            .then(function (response) {
                //console.log("BAs---",response.body);
                $scope.baCompanyList = angular.copy(response.data);
                $scope.baFlag = true;
            });
//$scope.baCompanyList = [{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-03-12T11:56:56+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1575,"BAPANNUMBER":null,"BACOMPANY":"SB Projects Consultant Pvt ltd","CREATEDBY":"vijayma"},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-03-12T11:56:57+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1576,"BAPANNUMBER":null,"BACOMPANY":"NNA Consultancy Services Private Limited","CREATEDBY":"vijayma"},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-03-12T11:56:58+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1577,"BAPANNUMBER":null,"BACOMPANY":"R K Venkatesan & Co","CREATEDBY":"vijayma"},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1578,"BAPANNUMBER":null,"BACOMPANY":"Crisil Inftrastructure","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1579,"BAPANNUMBER":null,"BACOMPANY":"Reliance","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-03-12T11:56:59+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1580,"BAPANNUMBER":null,"BACOMPANY":"VSR Associates","CREATEDBY":"vijayma"},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1581,"BAPANNUMBER":null,"BACOMPANY":"new company","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1582,"BAPANNUMBER":null,"BACOMPANY":"Harithasa Associates","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1583,"BAPANNUMBER":null,"BACOMPANY":"K K Kapoor and Associates","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1584,"BAPANNUMBER":null,"BACOMPANY":"Nashik","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1596,"BAPANNUMBER":null,"BACOMPANY":"M/s.GASM DANSR AND CO","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1597,"BAPANNUMBER":null,"BACOMPANY":"VCSME Ratings Pvt. Ltd","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1598,"BAPANNUMBER":null,"BACOMPANY":"Infosys","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1599,"BAPANNUMBER":null,"BACOMPANY":"G. Choudhury & Associates","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1600,"BAPANNUMBER":null,"BACOMPANY":"my company","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1601,"BAPANNUMBER":null,"BACOMPANY":"Premium Consultancy Pvt. Ltd","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1602,"BAPANNUMBER":null,"BACOMPANY":"Haribhatki & Co.","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1603,"BAPANNUMBER":null,"BACOMPANY":"PAMAC Finserve Private Limited","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1604,"BAPANNUMBER":null,"BACOMPANY":"Gopalaiyer And Subramanian","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1605,"BAPANNUMBER":null,"BACOMPANY":"R.Venkatakrishnan & Associates","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1606,"BAPANNUMBER":null,"BACOMPANY":"B. M. Chatrath & Co.","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1607,"BAPANNUMBER":null,"BACOMPANY":"Vector DSS Private Limited","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1608,"BAPANNUMBER":null,"BACOMPANY":"Nitya Credit Solutions Private Limited","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1609,"BAPANNUMBER":null,"BACOMPANY":"Agarwal & Saxena","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1610,"BAPANNUMBER":null,"BACOMPANY":"S. Ramanand Aiyar & Co.","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1611,"BAPANNUMBER":null,"BACOMPANY":"Nexify Services pvt. Ltd","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1612,"BAPANNUMBER":null,"BACOMPANY":"Talati & Talati","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1613,"BAPANNUMBER":null,"BACOMPANY":"Manohar Chowdhry & Associates","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1614,"BAPANNUMBER":null,"BACOMPANY":"Verifact Services Pvt. Ltd.","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1615,"BAPANNUMBER":null,"BACOMPANY":"N.S. Bhargava & Co.","CREATEDBY":null},{"BAADDRESS":null,"BACOUNTRYID":null,"BAREGIONID":null,"BAEXPIREDATE":null,"BAPINCODE":null,"MODIFIEDBY":null,"BACOMPANYCODE":null,"CREATEDON":"2018-08-17T13:01:27+0000","BASTATEID":null,"BACITYID":null,"BAFAXNUMBER":null,"BAEMAIL":null,"MODIFIEDON":null,"BAPHONENUMBER":null,"BAAGREEMENTDATE":null,"ISDELETED":"N","VERSION":0,"ID":1616,"BAPANNUMBER":null,"BACOMPANY":"X","CREATEDBY":null}]
        /// get BA Activity

        $http.post("/bonita/API/extension/getMasterData?f=baactivity", {})
            .then(function (response) {
                //console.log(response.body);
                $scope.baActivities = angular.copy(response.data);
                //  $scope.baActivitiesModel = angular.copy(response.data);
            });
            
            //console.log("BA Flag----",$scope.baFlag);
          
             //console.log(" $scope.properties.mandateData.smeBA-----",  $scope.properties.mandateData.smeBa)
          

        /// get Documents uploaded

        $http.post("/bonita/API/extension/getMasterData?f=getuploadedfile", { "mandateId": $scope.properties.mandateId })
            .then(function (response) {
                //console.log("Upload List ************");
                //console.log(response.body);

                $scope.uploadedList = angular.copy(response.data);
            });

        //Rest call for RM drop down

        //   $http.post("/bonita/API/extension/getMasterData?f=rm",{})
        //   .then(function(response){
        //      //console.log("!!!!!!!!!!!!!!!!!");
        //      $scope.rm = angular.copy(response.data);
        //     //console.log($scope.rm);
        //      //console.log("!!!!!!!!!!!!!!!!!");
        //   },function(error){
        //       //console.log(err);
        //   });


        //Scanned-Mandate
        $scope.documents = [];
        var scannedMandates = $scope.properties.mandateData.scannedMandate;
        var doc_array = scannedMandates.split(',');
        //console.log(doc_array);

        for (var i = 0; i < doc_array.length - 1; i++) {
            doc_array[i] = doc_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
            //console.log(doc_array[i]);
            $scope.documents.push(doc_array[i]);
        }
        //console.log("documents");

        //console.log($scope.documents);

        //   ********************************


        //Billing compnay Name Auto-complete 



        //object for Fee Master Rest call
        // var feeMaster = {
        //     "productName": $scope.properties.mandateData.productName,
        //     "institutionName": $scope.properties.mandateData.institutionName,
        //     "companyName": $scope.properties.mandateData.billingCompany
        // }
           var feeMaster = {
          
            "contact_full_name": $scope.properties.mandateData.billingCompanyContact,
            "company_name": $scope.properties.mandateData.billingCompany
        }


        //console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        $scope.baVendor =$scope.properties.mandateData.smeBa;
        console.log("@@@@@@@@@@@@@@$scope.baVendor@@@@@@@@@@@@@@@@@@",$scope.baVendor.length);
        if($scope.baVendor.length >0){
            $scope.showbaVendor = true;
        }
       

        //assigning productName, instituteName, billingCompany, billingCompanyConact to mandate object
     //   $scope.properties.mandate.productName = $scope.properties.mandateData.productName;
    //    $scope.properties.mandate.instituteName = $scope.properties.mandateData.institutionName;
    //    $scope.properties.mandate.billingCompany = $scope.properties.mandateData.billingCompany;
     //   $scope.properties.mandate.billingCompanyContact = $scope.properties.mandateData.billingCompanyContact;
        //Rest call for fee master data
  var feeMaster = {
          
            "contact_full_name": $scope.properties.mandateData.billingCompanyContact,
            "company_name": $scope.properties.mandateData.billingCompany
        }
        //console.log(feeMaster);
        $http.post("/bonita/API/extension/getMasterData?f=feeMaster", feeMaster)
            .then(function (response) {
                //console.log("$$$$$$$$$");
                //console.log(response.data);
                //console.log("$$$$$$$$$");
                //console.log(response.data[0].GST_NUMBER);
                $scope.properties.mandateData.gstNumber = response.data[0].GST_NUMBER;
                // $scope.properties.mandate.dealType = response.data[0].dealtype;
                $scope.properties.mandateData.locationOfService = response.data[0].LOCATION;
                $scope.properties.mandateData.gstExempt = response.data[0].GST_EXEMPT;
                $scope.properties.mandateData.sez = response.data[0].IS_SEZ_GST_FLAG;
                $scope.properties.mandateData.gstNumberNotApplicable = response.data[0].GST_NUMBER_NOT_APPLICABLE;
               // $scope.properties.mandate.companyCode = response.data[0].COMPANY_CODE
               // $scope.properties.mandate.companyContactId = response.data[0].CONTACTID
               // $scope.properties.mandate.gradingFees = response.data[0].RATINGFEE
               
               if( $scope.properties.mandateData.gstExempt =='N' &&  $scope.properties.mandateData.gstNumberNotApplicable == 'N'){
                   // $scope.properties.mandate.gstNumber = 'NA'   ;
                  $scope.gstApplicable = false;
                   //console.log("---$scope.properties.mandate.state ---",$scope.properties.mandateData.state );
                   if( $scope.properties.mandateData.billingCompanyContact && $scope.properties.mandateData.billingCompany ){
                       
        $http.post("/bonita/API/extension/getMasterData?f=bacompanygst", { "companyName": $scope.properties.mandateData.billingCompany,"contactName":  $scope.properties.mandateData.billingCompanyContact,"gst": response.data[0].IS_SEZ_GST_FLAG})
            .then(function (response) {
                $scope.gstNumbers = angular.copy(response.data);
                //console.log("GST NUMBERS");
                //console.log($scope.gstNumbers);
                //console.log("GST NUMBERS");
                  if($scope.gstNumbers && $scope.gstNumbers.length == 1){
                   $scope.properties.mandateData.gstNumber = $scope.gstNumbers[0].GST_Number ;
                   $scope.gstApplicable = true;  
                }
            }, function (error) {
                //console.log(err);
            });
        }    
            
                }else{
                    $scope.properties.mandateData.gstNumber = 'NA'   ;
                   $scope.gstApplicable = true;
                }
               
               
                
            }, function (error) {
                //console.log(error);
            });








        //   **************************

    }

$scope.getSelectedActvityValues = function (obj, index) {
    $scope.selectedActivities[index] = obj;
}
 
    $scope.onContactSelected = function (obj,bacontact, index) {
        console.log("Ba Contacts ---",bacontact);
         var newObj = {}
        if(obj){
            angular.forEach(bacontact, function (values, keys) {
                if(values.BANAME== obj){
              // newObj = JSON.parse(values);
              
               newObj = values
               }
               
            })
   
        }
        //console.log(obj, 'selectedContact');
       // var newObj = JSON.parse(obj);
        //console.log(newObj.BANAME, "&&&&&&");
        //console.log(newObj.WINDOWSID, "&&&&&&");
        var selectedContact = newObj.BANAME;
        var windowsID = newObj.WINDOWSID;
        console.log(windowsID, "windowsID");

        console.log("selectedContact ---",selectedContact);
        //console.log($scope.smeBa, 'indoreq');
        if ($scope.smeBa && $scope.smeBa.length > 0) {
            $scope.smeBa[index].contactName = selectedContact;
            $scope.smeBa[index].windowsId = windowsID;
            //console.log($scope.smeBa[index], 'indoreq');
        } else {
           $scope.smeBa = {
                name: '',
                contactName: selectedContact,
                windowsId: windowsID,
                smeBAActivity: '',
                smeBAScheduledDate: ''
            }
        }
        //console.log($scope.smeBa, $scope.smeBa.length);
        //console.log($scope.vendorContactName, 'vendorContactName');
    }


    var selectedActivitiesArray = [];
           var temp1 = [];
    var temp2 = [];
 
    
    $scope.onActivitySelected = function (args, i) {
        
          $scope.selectedActivities[i] = args.selectedActivity
        
console.log(i, "************************",args.selectedActivity);
//selectedActivities
        var index = i;
        //var selectedActivitiesArrayIndex = args.index;
  $scope.smeBa[index].smeBAActivity = args.selectedActivity;
  var dtFlag = false;
   angular.forEach(args.selectedActivity, function (values, keys) {
                if(values.activity== 'Site visit only'){
                 $scope.datePick[index].statu = true;
                 dtFlag = true;
               }
               if(!dtFlag){
                    $scope.datePick[index].statu = false;
               }
            })
   
        console.log($scope.smeBa, "*****$scope.smeBa***");
   
        //console.log("***********");

        //console.log($scope.smeBa, "out");
        //	//console.log($scope.vendorActivityName, 'vendorActivityName');
    }

    $scope.onCompanySelected = function (selectedBA, index) {
        /// get BA contact list
      //  console.log("$scope.properties.smeBa.smeBAActivity)---",selectedBA.smeBAActivity);
        // $scope.baVendor.push({});
         $scope.datePick.push({'statu':false});
      //  $scope.smeBa.push(Object.assign({}, $scope.smeBaItem));
        //console.log($scope.smeBa, 'indoreq');
        if (selectedBA) {//smeBAActivity
           $scope.smeBa[index] = selectedBA;
          $scope.smeBa[index].smeBAActivity = selectedBA.smeBAActivity;
            console.log('--BA--',$scope.smeBa[index] ,'--',index);
        } else {
            $scope.smeInfoReq = {
                name: selectedBA,
                contactName: '',
                windowsId: '',
                smeBAActivity: ''
            }
        }
        //console.log($scope.smeBa);
        //console.log(selectedBA, 'after');
        $http.post("/bonita/API/extension/getMasterData?f=bacontact", { 'baName': selectedBA.name })
            .then(function (response) {
                //console.log(response.data);

                $scope.baContactList[index] = angular.copy(response.data);
            })
    }
      $scope.onCompanySelectedForContact = function (selectedBA, index) {
           $http.post("/bonita/API/extension/getMasterData?f=bacontact", { 'baName': selectedBA })
            .then(function (response) {
                //console.log(response.data);

                $scope.baContactList[index] = angular.copy(response.data);
            })
      }
    
    
$scope.baScheduledDateFun = function (e,i) {
        //console.log("data is ",e.target.value);
 var index = i;
if ($scope.smeBa && $scope.smeBa.length > 0) {
            $scope.smeBa[index].smeBAScheduledDate = e;
            //console.log( $scope.smeBa[index].smeBAScheduledDate, 'Test');
        } else {
            $scope.smeBa = {
                name: selectedBA,
                contactName: '',
                windowsId: '',
                smeBAActivity: '',
                smeBAScheduledDate: ''
            }
        }
        
         //console.log("  $scope.smeBa---> ",  $scope.smeBa);
    }
//Entity Name drop-down select

    $scope.getEntitydetails = function (enitityname) {

        //console.log(enitityname);
        $http.post("/bonita/API/extension/getMasterData?f=entityname", { "entityName": enitityname })
            .then(function (response) {
                //console.log("!!!!!!!!!!!!!!!!!" + "********");
                //console.log(response.data);
                $scope.properties.mandate.entityName = response.data.company_name;
                $scope.properties.mandate.entityAddress = response.data.address1;
                //    $scope.properties.mandate.entityContactPerson = response.data.DEALERCONTACTPERSON;
                $scope.properties.mandate.entityContactPersonNumber = response.data.phone_num;
                     $scope.properties.mandate.entityDisplayName = response.data.display_company_name;
                       //  mandate.entityDisplayName = $scope.properties.mandateData.entityDisplayName;
                //  $scope.properties.mandate.entityEmailForCommunication = response.data.dealercommunication;
                //   $scope.properties.mandate.entityEmailRecord = response.data.DEALEREMAIL;
                //   $scope.properties.mandate.entityCity = response.data.DEALERLOCATION;
                //   $scope.properties.mandate.entityState = response.data.DEALERLOCATION;
                //   $scope.properties.mandate.promoterName = response.data.PROMOTERNAME;

            }, function (error) {
                //console.log(err);
            });
    }



    //method for getting Rm-Details

    //   $scope.getRmdetails = function(rm){

    //       $http.post("/bonita/API/extension/getMasterData?f=rmcontactname",{"rmContactName":rm})
    //       .then(function(response){
    //       //console.log("*****************");
    //       //console.log(response.data);
    //       $scope.properties.mandate.rmContactNumber = response.data.rm_contact_number;
    //       $scope.properties.mandate.rmEmail = response.data.rm_contact_email;
    //         //console.log("***************");
    //       },function(error){
    //           //console.log(err);
    //       });

    //   }     


    $scope.getProductType = function (type) {
        //console.log(type);

        //Rest call for get product Type
        $http.post("/bonita/API/extension/getMasterData?f=producttype", { "productType": type })
            .then(function (response) {
           ;
            }, function (error) {
                //console.log(err);
            });

    }




    var URL = "/bonita/portal/resource/process/SME%20Process/1.0/API/bpm/process/" + $scope.properties.processId + "/instantiation"
   


    $scope.next = function () {
        //console.log("in");
    }


    $scope.createMandate = function () {
        // $scope.properties.mandate.userId = $scope.properties.userId;
    var validateFlag = $scope.validateBAs();
		console.log("validateFlag----->",validateFlag)
	if(validateFlag){
       $scope.loading = true;
       $scope.opacity = "opacity: 0.1;"
        //console.log("$scope.properties.userId ::", $scope.properties.mandateData);
        let mandate = {};
        mandate.urgentCase = $scope.properties.mandateData.urgentCase;
        mandate.baApplicable = $scope.properties.mandateData.baApplicable;
        mandate.productName = $scope.properties.mandateData.productName;
        mandate.institutionName = $scope.properties.mandateData.institutionName;
        mandate.productType = $scope.properties.mandateData.productType;
        mandate.billingCompany = $scope.properties.mandateData.billingCompany;
        mandate.billingCompanyContact = $scope.properties.mandateData.billingCompanyContact;
        mandate.gstExempt = $scope.properties.mandateData.gstExempt;
        mandate.gstNumberNotApplicable = $scope.properties.mandateData.gstNumberNotApplicable;
        mandate.sez = $scope.properties.mandateData.sez;
        mandate.gstNumber = $scope.properties.mandateData.gstNumber;
        mandate.locationOfService = $scope.properties.mandateData.locationOfService;
        mandate.entityName = $scope.properties.mandateData.entityName;
        mandate.entityAddress = $scope.properties.mandateData.entityAddress;
        mandate.entityContactPersonNumber = $scope.properties.mandateData.entityContactPersonNumber;
        mandate.paymentDetails = $scope.properties.mandateData.paymentDetails;
        mandate.paymentAmount = $scope.properties.mandateData.paymentAmount;
        mandate.paymentDate = $scope.properties.mandateData.paymentDate;
        mandate.scannedMandate = $scope.properties.mandateData.scannedMandate;
        mandate.gradingFees = $scope.properties.mandateData.gradingFees;
        mandate.bdName = $scope.properties.mandateData.bdName;
         mandate.creditExposureByBank = $scope.properties.mandateData.creditExposureByBank;
         mandate.entityDisplayName = $scope.properties.mandateData.entityDisplayName;
        
        mandate.companyCode = $scope.properties.mandateData.companyCode;
        mandate.productCategory = $scope.properties.mandateData.productCategory;
        mandate.smeComment = $scope.properties.mandateData.smeComment;
        mandate.companyContactId = $scope.properties.mandateData.companyContactId;
        mandate.mandateOriginatedDate = $scope.properties.mandateData.mandateOriginatedDate;
        mandate.allocateToCommitteeDate = $scope.properties.mandateData.allocateToCommitteeDate;
        mandate.allocatedToCommittee = $scope.properties.mandateData.allocatedToCommittee;
        mandate.ismeCaseSubStatus = $scope.properties.mandateData.ismeCaseSubStatus;
        mandate.constitutionName = $scope.properties.mandateData.constitutionName; 
        mandate.smeBa = $scope.smeBa;

        //console.log('save mandate', '---', mandate);
        //console.log('smeBa', '---', $scope.smeBa);

        //console.log('save mandate', '---', mandate);
        var request = {
            "smeMandateInput": mandate,
            "netOpsMandateButtonAction": "Create",
            "noUserId": $scope.properties.userId
        }
        submitTask(request);
        //console.log(request);
	}
    }

    $scope.reloadDashBoard = function () {
        $window.parent.location.reload();
    }


    $scope.saveForlater = function () {
        // $scope.properties.mandate.userId = $scope.properties.userId;
 var validateFlag = $scope.validateBAs();
		console.log("validateFlag----->",validateFlag)
		if(validateFlag){
        //console.log("$scope.properties.userId ::", $scope.properties.mandateData);
        let mandate = {};
        mandate.urgentCase = $scope.properties.mandateData.urgentCase;
        mandate.baApplicable = $scope.properties.mandateData.baApplicable;
        mandate.productName = $scope.properties.mandateData.productName;
        mandate.institutionName = $scope.properties.mandateData.institutionName;
        mandate.productType = $scope.properties.mandateData.productType;
        mandate.billingCompany = $scope.properties.mandateData.billingCompany;
        mandate.billingCompanyContact = $scope.properties.mandateData.billingCompanyContact;
        mandate.gstExempt = $scope.properties.mandateData.gstExempt;
        mandate.gstNumberNotApplicable = $scope.properties.mandateData.gstNumberNotApplicable;
        mandate.sez = $scope.properties.mandateData.sez;
        mandate.gstNumber = $scope.properties.mandateData.gstNumber;
        mandate.locationOfService = $scope.properties.mandateData.locationOfService;
        mandate.entityName = $scope.properties.mandateData.entityName;
        mandate.entityAddress = $scope.properties.mandateData.entityAddress;
        mandate.entityContactPersonNumber = $scope.properties.mandateData.entityContactPersonNumber;
        mandate.paymentDetails = $scope.properties.mandateData.paymentDetails;
        mandate.paymentAmount = $scope.properties.mandateData.paymentAmount;
        mandate.paymentDate = $scope.properties.mandateData.paymentDate;
        mandate.scannedMandate = $scope.properties.mandateData.scannedMandate;
        mandate.gradingFees = $scope.properties.mandateData.gradingFees;
        mandate.bdName = $scope.properties.mandateData.bdName;
            mandate.entityDisplayName = $scope.properties.mandateData.entityDisplayName;
         mandate.creditExposureByBank = $scope.properties.mandateData.creditExposureByBank;
    
        mandate.companyCode = $scope.properties.mandateData.companyCode;
        mandate.productCategory = $scope.properties.mandateData.productCategory;
        mandate.smeComment = $scope.properties.mandateData.smeComment;
        mandate.companyContactId = $scope.properties.mandateData.companyContactId;
        mandate.mandateOriginatedDate = $scope.properties.mandateData.mandateOriginatedDate;
        mandate.allocateToCommitteeDate = $scope.properties.mandateData.allocateToCommitteeDate;
        mandate.allocatedToCommittee = $scope.properties.mandateData.allocatedToCommittee;
        mandate.ismeCaseSubStatus = $scope.properties.mandateData.ismeCaseSubStatus;
        mandate.constitutionName = $scope.properties.mandateData.constitutionName; 
        mandate.smeBa = $scope.smeBa;
        //console.log('save mandate', '---', mandate);
        //console.log('smeBa', '---', $scope.smeBa);

        //console.log('save mandate', '---', mandate);
        var request = {
            "smeMandateInput": mandate,
            "netOpsMandateButtonAction": "SaveForLater",
            "noUserId": $scope.properties.userId
        }
        submitTask(request);
        //console.log(request);
        }
    }

    $scope.reloadDashBoard = function () {
        $window.parent.location.reload();
    }

   $scope.rejectComment = function(){
       
          $('#functionUsersCheck').modal('show');
    
   }



    $scope.reject = function () {
        // $scope.properties.mandate.userId = $scope.properties.userId;
  
        //console.log("$scope.properties.userId ::", $scope.properties.mandateData);
      let mandate = {};
        mandate.urgentCase = $scope.properties.mandateData.urgentCase;
        mandate.baApplicable = $scope.properties.mandateData.baApplicable;
        mandate.productName = $scope.properties.mandateData.productName;
        mandate.institutionName = $scope.properties.mandateData.institutionName;
        mandate.productType = $scope.properties.mandateData.productType;
        mandate.billingCompany = $scope.properties.mandateData.billingCompany;
        mandate.billingCompanyContact = $scope.properties.mandateData.billingCompanyContact;
        mandate.gstExempt = $scope.properties.mandateData.gstExempt;
        mandate.gstNumberNotApplicable = $scope.properties.mandateData.gstNumberNotApplicable;
        mandate.sez = $scope.properties.mandateData.sez;
        mandate.gstNumber = $scope.properties.mandateData.gstNumber;
        mandate.locationOfService = $scope.properties.mandateData.locationOfService;
        mandate.entityName = $scope.properties.mandateData.entityName;
        mandate.entityAddress = $scope.properties.mandateData.entityAddress;
        mandate.entityContactPersonNumber = $scope.properties.mandateData.entityContactPersonNumber;
        mandate.paymentDetails = $scope.properties.mandateData.paymentDetails;
        mandate.paymentAmount = $scope.properties.mandateData.paymentAmount;
        mandate.paymentDate = $scope.properties.mandateData.paymentDate;
        mandate.scannedMandate = $scope.properties.mandateData.scannedMandate;
        mandate.gradingFees = $scope.properties.mandateData.gradingFees;
        mandate.bdName = $scope.properties.mandateData.bdName;
            mandate.entityDisplayName = $scope.properties.mandateData.entityDisplayName;
         mandate.creditExposureByBank = $scope.properties.mandateData.creditExposureByBank;
    
        mandate.companyCode = $scope.properties.mandateData.companyCode;
        mandate.productCategory = $scope.properties.mandateData.productCategory;
        mandate.smeComment = $scope.properties.mandateData.smeComment;
        mandate.companyContactId = $scope.properties.mandateData.companyContactId;
        mandate.mandateOriginatedDate = $scope.properties.mandateData.mandateOriginatedDate;
        mandate.allocateToCommitteeDate = $scope.properties.mandateData.allocateToCommitteeDate;
        mandate.allocatedToCommittee = $scope.properties.mandateData.allocatedToCommittee;
        mandate.ismeCaseSubStatus = $scope.properties.mandateData.ismeCaseSubStatus;
        mandate.constitutionName = $scope.properties.mandateData.constitutionName; 
        mandate.smeBa = $scope.smeBa;
          //console.log('save mandate', '---', mandate);
        //console.log('smeBa', '---', $scope.smeBa);

        //console.log('save mandate', '---', mandate);
        var request = {
            "smeMandateInput": mandate,
            "netOpsMandateButtonAction": "Rejected",
            "noUserId": $scope.properties.userId
        }
        submitTask(request);
        //console.log(request);

    }

    $scope.reloadDashBoard = function () {
        $window.parent.location.reload();
    }

    /**
    * Execute a get/post request to an URL
    * It also bind custom data from success|error to a data
    * @return {void}
    */
    function doRequest(method, url, params, data) {
        // vm.busy = true;
        var req = {
            method: method,
            url: url,
            data: angular.copy(data),
            params: params
        };

    }

    function submitTask(data) {
        //console.log(data);
        var id;
        id = $scope.properties.taskId;
        if (id) {
            $http.put('../API/bpm/humanTask/' + id, { 'assigned_id': $scope.properties.userId }).then(function (response) {
                //console.log(response, 'res task assign');
                $http.post('../API/bpm/userTask/' + id + '/execution', data, { 'user': $scope.properties.userId }).then(function (response) {
                    //console.log(response, 'res');
                    $scope.submitDocuments($scope.properties.mandateId);
                   // $timeout(function () { $window.parent.location.reload(); }, 1500);
                }, function (error) {
                    //console.log(error);
                });

            }, function (error) {
                //console.log(error);
            });


        } else {
            $log.log('Impossible to retrieve the task id value from the URL');
        }
    }
   $scope.validateBAs = function(){
    var vbNameFlag = false;
	var vbContactFlag = false;
	var vbActivityFlag = false;
	var vbDuplicate = false;
	var vbSiteVisit = false;
	//contactName: '',
    //smeBAActivity: ''
	
     angular.forEach($scope.smeBa, function ( values, keys) {
	 console.log("values.smeBAActivity.length ---",values.smeBAActivity)
               if(values.name == '' || values.name == undefined){
                  vbNameFlag = true;
               }else if(values.contactName == '' || values.contactName == undefined){
				  vbContactFlag = true; 
			   }else if(values.smeBAActivity == undefined || values.smeBAActivity.length == 0){
				   vbActivityFlag = true;
			   }
			   //removed site-visit field
			 //  else if(values.smeBAActivity != undefined || values.smeBAActivity.length > 0){
				   
				//      angular.forEach(values.smeBAActivity, function ( value, key) {
    //               if(value.activity == 'Site visit only' && values.smeBAScheduledDate == ""  ){
    //                   vbSiteVisit = true;
    //                   }   
    //              }) 
				   
				 
			 //  }
			   else{
				   
			   }
              
            })
             angular.forEach($scope.smeBa, function ( values, keys) {
                 
                 angular.forEach($scope.smeBa, function ( value, key) {
                    if(values.name != '' && value.name != '' &&  values.name == value.name &&  values.contactName == value.contactName && keys != key  ){
                          vbDuplicate = true;
               }  
                 }) 
             })  
			
			 if(vbNameFlag){
                  $scope.pop("Please select BA Vendor name")
				  return false;
               }
			    if(vbContactFlag){
                  $scope.pop("Please select BA Contact name")
				   return false;
               }
			    if(vbActivityFlag){
                   $scope.pop("Please select BA Activities") 
				    return false;
               }
                /*if(vbDuplicate){
                   $scope.pop("Duplicate BA contact names found and Please select someother vendor name") 
                   return false;
               }if(vbSiteVisit){
                    $scope.pop("Please select Site visit date") 
				    return false;
               }*/
			    return true;
}
    //toast message for Approved
          $scope.pop = function(data){
              console.log("in pop");
              toaster.success({title: "Success", body:data});
              toaster.pop('error', "Email Sended", '<ul><li>Render html</li></ul>', null, 'trustedHtml', 5000, 'trustedHtml', function(toaster) {
                  var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
                  if (match) $window.open(match[0]);
                  return true;
              });        
  };
  
  $scope.baApplicable = function (choice) {
            console.log(choice);
            if (choice == 'true' || choice == true) {
               console.log("Inside baApplicable if");
                $scope.showbaVendor = true;
                    $scope.baVendor = [{}];
                $scope.smeBa = [{
                    name: '',
                    contactName: '',
                    windowsId: '',
                    smeBAActivity: '',
                    smeBAScheduledDate: ''
                 }];
            } else {
               //console.log($scope.properties.mandate.baApplicable);
                $scope.showbaVendor = false;
              //  $scope.smeBa = [];
                 $scope.smeBa = [];
     
             $scope.datePick = [{
                   statu :''
               }];
            $scope.smeBaItem = {
                name: '',
                contactName: '',
                windowsId: '',
                smeBAActivity: [], 
                smeBAScheduledDate: ''
            };
          
              $scope.selectedActivities = [];
              $scope.baActivitiesModel = [];
            }
        }
        
    $scope.bdRejectionHistory = function(){
          $http.post("/bonita/API/extension/mandateUtility?f=updateBDRejectedLog", { "globalMandateId": $scope.properties.mandateData.globalMandateId,"comments":$scope.properties.mandateData.smeComments })
            .then(function (response) {
                console.log(" updateBDRejectedLog",response);
                // $scope.poNumber = angular.copy(response.data);
                if(response.data){
                    $scope.pop("Rejected Successfully")
                }
                //console.log($scope.poNumber);
                //console.log("***************");
            }, function (error) {
                //console.log(err);
            });
    }
    
    
     $scope.getComments = function(userRole){
   console.log("in comments");
  $http.get("/bonita/API/extension/smekpi?f=getcomments&c="+$scope.properties.mandateData.mandateId+"&p=NO&t="+userRole)
  .then(function (response) {
               console.log(response);
               var temp = response.data;
               console.log(temp.dashBoardDataList);
                $scope.commentsObj = angular.copy(temp.dashBoardDataList);
                  console.log( $scope.commentsObj,"comments");
            });
  }  
  
  $scope.setComment = function(comment,targetRole){
      console.log(comment);
      if( comment == " " ||  comment === undefined){
          $scope.pop("Empty comments not allowed") 
      }else if(targetRole === undefined || targetRole == " "){
          
      }
      else{
          
          if(targetRole == 'Info Req-Entity'){
              var formData = new FormData();
                    formData.append('entityId', $scope.properties.mandateData.companyCode);
                    formData.append('mandateId',  $scope.properties.mandateData.globalMandateId);
                     formData.append('userName', "From Bonita");
                    formData.append('comment',  comment);
                  $http.post("/bonitaAPI/accesssme/entity/comment", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                        })
                        .error(function (error) {
                        });
          }
      
      $http.post("/bonita/API/extension/smedashboard?f=savecomments",{
          "comment":comment,
           "userRole":"NO", 
           "toUserRole":targetRole,
           "workflow_id":$scope.properties.mandateData.mandateId,
          "globalMandateId":$scope.properties.mandateData.globalMandateId

      })
      .then(function (response) {
          $scope.properties.mandateData.userComment = " ";
          console.log(response.data);
            $scope.pop("Comment posted successfuly")
          $scope.getComments(targetRole);
          
      })
      }
  }
        
           var  typingBC = false;
 $scope.getBillingCompanyCon = function(e){
   $scope.zIndexC = 0;  
    if(!typingBC){
          typingBC = true;
          $scope.enFlagNew1 = false;
          console.log(typingBC);
              $timeout(function(){
                  typingBC = false;
                    console.log($scope.properties.mandateData.billingCompany);
               
                       $scope.properties.mandateData.gstNumber ="";
                        $scope.gstApplicable = true;  
                    //  $scope.properties.mandateData.companyCode ="";
                       $scope.properties.mandateData.gstNumber ="";
                        $scope.properties.mandateData.gstExempt ="";
                    $scope.properties.mandateData.sez ="";
                    $scope.properties.mandateData.gstNumberNotApplicable ="";
                            $scope.billingCompanyContacts = [];
                            console.log("in");
                            $http.post("/bonita/API/extension/getMasterData?f=bacompanycontact",{"companyName":$scope.properties.mandateData.billingCompany,"contactName":$scope.properties.mandateData.billingCompanyContact})
                            .then(function(response){
                                   console.log("In billingCompanyContacts");
                            if(response.data){
                                 $scope.zIndexC = 1;
                                   $scope.billingCompanyContacts = angular.copy(response.data);
                                   if($scope.billingCompanyContacts.length == 0){
                                        $scope.zIndexC = 0;
                                   }
                            }else{
                    
                                $scope.properties.mandateData.gstNumber ="";
                            }
                            console.log($scope.billingCompanyContacts);
                            },function(error){
                            console.log(err);
                        });
                  
              },1000)
              }
         

}
        
}
