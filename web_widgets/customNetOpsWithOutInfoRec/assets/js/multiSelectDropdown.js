angular.module("bonitasoft.ui.widgets").directive('multiselectDropdown', function () {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
            dataselected: '=',
            i: '=',
            onActivitySelected: '&onActivitySelected'
            
        },
        template:
            "<div class='form-control btn-group' data-ng-class='{open: open}' style='width: 100%;height: 34px;'>" +
            "<button class='btn btn-small' data-ng-click='openDropdown()' style='background: transparent;border: none;width: inherit;box-shadow: none;'>----choose activities----</button>" +
            "<button class='btn btn-small dropdown-toggle' data-ng-click='openDropdown()' style='background: transparent;border: none;box-shadow: none;'></button>" +
            "<ul class='dropdown-menu ba-activity dropdown-length' aria-labelledby='dropdownMenu' style='position: absolute; width: 100%;'>" +
            "<li style='cursor:pointer;' data-ng-repeat='option in options'><a data-ng-click='toggleSelectItem(option)'><span data-ng-class='getClassName(option)' aria-hidden='true'></span> {{option.ACTIVITY}}</a></li>" +
            "</ul>" +
            "</div>",

        controller: function ($scope) {
            
            
            $scope.model.push([]);
            
        
            $scope.openDropdown = function () {

                $scope.open = !$scope.open;

            };
            
          angular.forEach($scope.dataselected, function (val, id) {
                     
                        val.ACTIVITY = val.activity;
                      $scope.model[$scope.i].push(val);
                     
   });
   console.log("$scope.model------",$scope.model)
          
            var temp1 = []
            var selectedActivitiesArray =[]
       
            $scope.toggleSelectItem = function (option) {

                 //console.log($scope.i, "index in directive");
 //console.log("$scope.i---", $scope.i);
                var intIndex = -1;
                
               // console.log(option ,"===option===")

                angular.forEach($scope.model[$scope.i], function (item, index) {

                    if (item.ACTIVITY == option.ACTIVITY) {
   
                       // console.log("found");
                        
                           intIndex = index;

                    }

                });

                if (intIndex >= 0) {

                    $scope.model[$scope.i].splice(intIndex, 1);
                    angular.forEach(temp1, function (values, keys) {
                    if (values.activity == option.ACTIVITY) {
                        console.log(keys);
                        console.log("VALUE ISS", values);
                        temp1.splice(keys, 1);
                    }
                });

                } else {
                    
                   var addSelectedActivities = {
                                "activity": option.ACTIVITY
                            }
                   
                
                 //   selectedActivitiesArray.push(addSelectedActivities)
                   // temp1 = [...$scope.smeBa[index].smeBAActivity]
                    temp1.push(addSelectedActivities);
                
                  option.activity= option.ACTIVITY;
                    $scope.model[$scope.i].push(option);

                }
                
             
              // console.log(intIndex,"updated INdex");
                 
                var obj = {
                    "selectedActivity" :  $scope.model[$scope.i],
                    "index" : intIndex,
                    "state": option.state
                    
                }
                 $scope.onActivitySelected({args : obj});
                  
                  
               // console.log(   $scope.smeBa,"====SME BA");
               //   $scope.onActivitySelected( temp1);
                 console.log(temp1,"----temp1 in dircetive");
               

            };

            $scope.getClassName = function (option) {
                
         
            // console.log(option,"---------Inside class selected");

                var varClassName = 'glyphicon glyphicon-remove-circle deselect';
                
                option.state = true;
                var sleArry = [];
             //   if($scope.model[$scope.i] == 0){
                 angular.forEach($scope.dataselected, function (val, id) {
                     if (val.activity == option.ACTIVITY ) {
                         // sleArry.push(val); 
                        option.state = false;
                        
                      //$scope.model[$scope.i].push(option);
                  //    $scope.dataselected.splice(id, 1);
                
                        varClassName = 'glyphicon glyphicon-ok-circle select';
                     }
   });
             //   }    
          //  console.log(temp1,"----temp1 in dircetive");
             //console.log($scope.model[$scope.i],"----Array in dircetive");
                angular.forEach($scope.model[$scope.i], function (item, index) {
                    
              //  console.log(item.ID ,"===item.ID ====");
               // console.log(option.ID ,"===option.ID ====");
                    if (item.ACTIVITY == option.ACTIVITY) {
                        
                        
                      //  console.log("##################33333");
                        
                        option.state = false;
                        
      
                        varClassName = 'glyphicon glyphicon-ok-circle select';
                     

                    }

                });

                return (varClassName);

            };
            
           

       // console.log("BAS LI---", $scope.smeBa );
        }
    }

});