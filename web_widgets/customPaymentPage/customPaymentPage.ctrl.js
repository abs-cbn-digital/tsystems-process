/**
 * The controller is a JavaScript function that augments the AngularJS scope and exposes functions that can be used in the custom widget template
 * 
 * Custom widget properties defined on the right can be used as variables in a controller with $scope.properties
 * To use AngularJS standard services, you must declare them in the main function arguments.
 * 
 * You can leave the controller empty if you do not need it.
 */
function ($scope,$http,$q, $window,$timeout) {
    $scope.isTotAmoutPayable = true;
    $scope.isAmount = true;
    $scope.isPoP = true;
    $scope.isCWT = true;
    $scope.refundLength= 0;
     $scope.memoLength= 0;
    $scope.isChecked =false;
       let apiHostName = "http://localhost:9004";
    $scope.creditableEnable = function(cs){
        console.log("-payment.creditWithHoldingTax-",cs)
       //    console.log("-ss-",$scope.isChecked)
    }
    
          $scope.submitTelecostDocuments = function (camp,file,userId) {
           console.log("campaign---",camp);
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                   var doc = file;
                   var formData = new FormData();
                     formData.append('file', doc);
                    formData.append('sourceId', camp.campaignId);
                     formData.append('sourceType', "Campaign");
                    formData.append('userName', userId);
                     formData.append('details',"Campaign_Details");
                     formData.append('docType', "Telecast_Order");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    }).then(function(res){ return res});
                        // .success(function (response) {
                        //     console.log(response);
                        //     console.log(response.successful);
                        //     $window.parent.location.reload();
                        // })
                        // .error(function (error) {
                        //     console.log(error);
                        // });
            }else{
               $timeout(function () { 
                    $scope.loading = false;
                    $window.parent.location.reload(); }, 2000);
            }
        }
           $scope.submitCreditableWithholdingTaxDocuments = function (camp,file,userId) { console.log('hhhh')
               if(camp.payment.creditWithHoldingTax){
           console.log("campaign---",camp);
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                   var doc = file;
                   var formData = new FormData();
                     formData.append('file', doc);
                    formData.append('sourceId', camp.campaignId);
                     formData.append('sourceType', "Campaign");
                    formData.append('userName', userId);
                     formData.append('details',"Campaign_Payment");
                     formData.append('docType', "Creditable_Withholding_Tax");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                    .then(function(value) {return value});
                        // .success(function (response) {
                        //     console.log(response);
                        //     console.log(response.successful);
                        //     $window.parent.location.reload();
                        // })
                        // .error(function (error) {
                        //     console.log(error);
                        // });
            }else{
               $timeout(function () { 
                    $scope.loading = false;
                    $window.parent.location.reload(); }, 2000);
            }
        }
           }
        
           function submitProofOfPaymentDocuments(camp,file,userId) {
           console.log("campaign---",camp);
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                   var doc = file;
                   var formData = new FormData();
                        formData.append('file', doc);
                    formData.append('sourceId', camp.campaignId);
                     formData.append('sourceType', "Campaign");
                    formData.append('userName', userId);
                     formData.append('details',"Campaign_Payment");
                     formData.append('docType', "Proof_Of_Payment");
                   
                   return $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            return response;
                            //$window.parent.location.reload();
                        })
                        .error(function (error) {
                            console.log(error);
                            return error;
                        });
            }
        }
        
        $scope.fileProofPaymentSelected = function (element) {
            $scope.proofofPayment = element.files[0];
            console.log($scope.proofofPayment);
        }
        
         $scope.fileCWTSelected = function (element) {
           $scope.cwt = element.files[0];
           console.log($scope.cwt);
        }
        
        function isAmountValid(){
           if ($scope.properties 
                && $scope.properties.campaign 
                && $scope.properties.campaign.payment 
                && $scope.properties.campaign.payment.amount !== null
                && $scope.properties.campaign.payment.amount !== '') {
                    $scope.isAmount = true;
                    return false;
            } else {
                $scope.isAmount = false;
                return false;
            }
        }
        function isTotalPayAmountValid(){
           if ($scope.properties 
                && $scope.properties.campaign 
                && $scope.properties.campaign.payment 
                && $scope.properties.campaign.payment.totalPayableAmount !== null
                && $scope.properties.campaign.payment.totalPayableAmount !== '') {
                    $scope.isTotAmoutPayable = true;
                    return false;
            } else {
                $scope.isTotAmoutPayable = false;
                return true;
            }
        }
        function isPoPValid(){console.log($scope.proofofPayment, '$scope.proofofPayment')
            $scope.isPoP = true;
            if($scope.proofofPayment === undefined 
                || $scope.proofofPayment === 'undefined'
                || !$scope.proofofPayment){
                    $scope.isPoP = false;
                }
        }
        function isCWTValid(){
            $scope.isCWT = true;
            console.log($scope.properties.campaign.payment.creditWithHoldingTax, 'tax')
            if($scope.properties.campaign.payment.creditWithHoldingTax === false || $scope.properties.campaign.payment.creditWithHoldingTax === null || $scope.properties.campaign.payment.creditWithHoldingTax === null){
                 $scope.isCWT = true;
            } else {
            if($scope.cwt === undefined 
                || $scope.cwt === 'undefined'
                || !$scope.cwt){
                    $scope.isCWT = false;
                }
            }
        }
        
   
        $scope.submitTask = function(camp, userid) { 
            isAmountValid();
            isTotalPayAmountValid();
            isPoPValid();
            isCWTValid();
            
        // check validation
        if($scope.isTotAmoutPayable &&
             $scope.isAmount &&
                $scope.isCWT &&
                    $scope.isPoP) {
                        var doc = $scope.proofofPayment;
                        var formData = new FormData();
                        formData.append('file', doc);
                        formData.append('sourceId', camp.campaignId);
                        formData.append('sourceType', "Campaign");
                        formData.append('userName', userid);
                        formData.append('details',"Campaign_Payment");
                        formData.append('docType', "Proof_Of_Payment");
                        
                        return $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                                transformRequest: angular.identity,
                                headers: { 'Content-Type': undefined }
                        })
                        .success(function (response) {
                        	console.log(response, 'ressssssssssss');
                        	console.log($scope.properties.campaign.payment.creditWithHoldingTax, 'tax')
                        	if(response.successful && $scope.properties.campaign.payment.creditWithHoldingTax){
                        	    
                                var formDataa = new FormData();
                                formDataa.append('file', $scope.cwt);
                                formDataa.append('sourceId', camp.campaignId);
                                formDataa.append('sourceType', "Campaign");
                                formDataa.append('userName', userid);
                                formDataa.append('details',"Campaign_Payment");
                                formDataa.append('docType', "Creditable_Withholding_Tax");
                                
                                $http.post(apiHostName+"/api/v1/s3/upload", formDataa, {
                                transformRequest: angular.identity,
                                headers: { 'Content-Type': undefined }
                                })
                                .success(function (result) {
                                    console.log(result);
                                    if(result.successful){
                                        $scope.submitPaymentTask(camp);
                                    }
                                })
                                .error(function (error) {
                                    console.log(error);
                                });
                        	}  else {
                        	    $scope.submitPaymentTask(camp)
                        	}
                        	
                        	//$window.parent.location.reload();
                        })
                        .error(function (error) {
                        	console.log(error);
                        	return error;
                        });
                       
        } else {
            // alert($scope.isCWT);
            if($scope.isAmount === false) {$scope.isAmount === false} else {$scope.isAmount === true};
            if($scope.isTotAmoutPayable === false) {$scope.isTotAmoutPayable === false} else {$scope.isTotAmoutPayable === true};
            if($scope.isPoP === false) {$scope.isPoP === false} else {$scope.isPoP === true};
            if($scope.isCWT === false) {$scope.isCWT === false} else {$scope.isCWT === true};
        }
       return;
        
    }
    
    $scope.submitPaymentTask = function (camp) {
        var id;
        id = $scope.properties.taskId ;
        
         var data = {
            "campaignInput": camp,
            "userId": $scope.properties.userId
        }
        if (id) {
            $http.put('../API/bpm/humanTask/' + id, { 'assigned_id': $scope.properties.userId }).then(function (response) {
                //console.log(response, 'res task assign');
                $http.post('../API/bpm/userTask/' + id + '/execution', data, { 'user': $scope.properties.userId }).then(function (response) {
                }, function (error) {
                    //console.log(error);
                });

            }, function (error) {
                //console.log(error);
            });


        } else {
            console.log('Impossible to retrieve the task id value from the URL');
        }
    }

}