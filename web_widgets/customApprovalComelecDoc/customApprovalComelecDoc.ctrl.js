/**
 * The controller is a JavaScript function that augments the AngularJS scope and exposes functions that can be used in the custom widget template
 * 
 * Custom widget properties defined on the right can be used as variables in a controller with $scope.properties
 * To use AngularJS standard services, you must declare them in the main function arguments.
 * 
 * You can leave the controller empty if you do not need it.
 */
function ($scope, $http, $window, $timeout, $filter ) {
    var white = 'white';
        $scope.documents = []
        $scope.officalReceiptDoc = {}
        $scope.creaditableDoc = {}
        $scope.signedTermsDoc = {}
        $scope.polAdsDoc = {}
        $scope.proofOfPayment = {}
        $scope.authorityDoc = {}
        $scope.telecostOrderDoc = {}
          $scope.agencyLetterDoc = {}
        $scope.specialPowerAttrDoc = {}
        $scope.isCommentError = false;
      $scope.apiHostName = "http://localhost:9004";
    $scope.init = function(){
             $http.get($scope.apiHostName+"/api/v1/document?sourceId="+$scope.properties.campaign.campaignId +"&sourceName=Campaign", {})
                .then(function (response) {
                 console.log("response-->",response)
                    $scope.documents = angular.copy(response.data.payload);
                    
                    if($scope.documents && $scope.documents.length > 0 ){
                       for(var i=0;i< $scope.documents.length ; i++){
                            var docu = $scope.documents[i];
                            if(docu.docType === "Telecast_Order" ){
                                $scope.telecostOrderDoc = docu;
                            }
                            if(docu.docType === "Official_Receipt" ){
                                $scope.officalReceiptDoc = docu;
                            }
                            if(docu.docType === "Official_Receipt" ){
                                $scope.officalReceiptDoc = docu;
                            }
                            if(docu.docType === "Creditable_Withholding_Tax" ){
                                $scope.creaditableDoc = docu;
                            }
                            if(docu.docType === "Original_Signed_Terms_And_Conditions" ){
                                $scope.signedTermsDoc = docu;
                            }
                            if(docu.docType === "Original_Signed_PolAd_Contract" ){
                                $scope.polAdsDoc = docu;
                            }
                            if(docu.docType === "Proof_Of_Payment" ){
                                $scope.proofOfPayment = docu;
                            }
                              if(docu.docType === "Telecast_Order" ){
                                $scope.telecostOrderDoc = docu;
                            }
                             if(docu.docType === "Agency_Letter" ){
                                $scope.agencyLetterDoc = docu;
                            }
                              if(docu.docType === "Special_Power_Of_Attorney" ){
                                $scope.specialPowerAttrDoc = docu;
                            }
                        }  
                    }
                    
                });
    }
    
    $scope.submitTask = function (comment,buttonAction) {
        
           if($scope.comment === undefined || $scope.comment === 'undefined' || !$scope.comment){
        $scope.isCommentError = true;
    } else {
        $scope.isCommentError = false;
      var id;
        id = $scope.properties.taskId ;
        
         var data = {
            "commentVar": comment,
            "userId": $scope.properties.user.userName,
            "buttonAction":buttonAction
        }
        if (id) {
            $http.put('../API/bpm/humanTask/' + id, { 'assigned_id': $scope.properties.user.userId }).then(function (response) {
                $http.post('../API/bpm/userTask/' + id + '/execution', data, { 'user': $scope.properties.user.userId }).then(function (response) {
                }, function (error) {
                });
            }, function (error) {
                //console.log(error);
            });
        } else {
            console.log('Impossible to retrieve the task id value from the URL');
        }
    }
       
    }
}