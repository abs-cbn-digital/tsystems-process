/**
 * The controller is a JavaScript function that augments the AngularJS scope and exposes functions that can be used in the custom widget template
 * 
 * Custom widget properties defined on the right can be used as variables in a controller with $scope.properties
 * To use AngularJS standard services, you must declare them in the main function arguments.
 * 
 * You can leave the controller empty if you do not need it.
 */
function ($scope,$http, $window,$timeout) {
     let apiHostName = "http://localhost:9004";
    $scope.refundLength= 0;
    $scope.memoLength= 0;
    $scope.isPolAdContrctSuccess = false;
    $scope.isOriginalSignedTermsConditnsSuccess = false;
    $scope.isOfficialReceiptSuccess = false;
    $scope.isAgencyLetterSuccess = false;
    $scope.isSpecialPowerSuccess = false;
    $scope.isTelecastorderSuccess = false;
    $scope.isPolAdContrctError = false;
    $scope.isOriginalSignedTermsConditnsError = false;
    $scope.isOfficialReceiptError = false;
    $scope.isAgencyLetterError = false;
    $scope.isSpecialPowerError = false;
    $scope.isTelecastorderError = false;
    $scope.buttonAction = '';
     
     
     $scope.filePolAdContractSelected = function (element) {
        $scope.polAdContract = element.files[0];
        console.log($scope.polAdContract);
    }
    
    $scope.fileOriginalSignTermsConditionsSelected = function (element) {
        $scope.originalSignedTermsConditions = element.files[0];
        console.log($scope.originalSignedTermsConditions,'sds00000');
    }
    
    $scope.fileOfficialReceiptSelected = function(element){
        $scope.officialReceipt = element.files[0];
         console.log($scope.officialReceipt,'officialReceipt');
    }
    
    $scope.fileAgencyLetterSelected = function(element){
        $scope.agencyLetter = element.files[0];
         console.log($scope.agencyLetter,'agencyLetter');
    }
      
    $scope.fileSpecialPowerOfAttorneySelected = function(element){
        $scope.specialPowerofAttorney = element.files[0];
         console.log($scope.specialPowerofAttorney,'specialPowerofAttorney');
    }
    
    $scope.fileTelecastOrderSelected = function(element){
        $scope.telecastOrder = element.files[0];
         console.log($scope.telecastOrder,'telecastOrder');
    }
    
       
        
        $scope.submitPolAdsContractDocuments = function (camp,userId) {
            let file = $scope.polAdContract;
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                    let doc = file;
                    let formPACData = new FormData();
                    formPACData.append('file', doc);
                    formPACData.append('sourceId', camp.campaignId);
                    formPACData.append('sourceType', "Campaign");
                    formPACData.append('userName', userId);
                    formPACData.append('details',"Campaign_Payment");
                    formPACData.append('docType', "Original_Signed_PolAd_Contract");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formPACData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            $scope.isPolAdContrctSuccess = true;
                            $scope.isAPIcallsCompleted(camp,userId);
                        })
                        .error(function (error) {
                            console.log(error);
                            $scope.isPolAdContrctSuccess = false;
                        });
            }else{
               
            }
        }
        
        $scope.submitSignedTermsAndConditionsDocuments = function (camp,userId) {
           console.log("campaign---",camp);
           
            let file = $scope.originalSignedTermsConditions;
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                    let doc = file;
                    let formOSDData = new FormData();
                    formOSDData.append('file', doc);
                    formOSDData.append('sourceId', camp.campaignId);
                    formOSDData.append('sourceType', "Campaign");
                    formOSDData.append('userName', userId);
                    formOSDData.append('details',"Campaign_Payment");
                    formOSDData.append('docType', "Original_Signed_Terms_And_Conditions");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formOSDData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            $scope.isOriginalSignedTermsConditnsSuccess = true;
                            $scope.isAPIcallsCompleted(camp,userId);
                        })
                        .error(function (error) {
                            console.log(error);
                            $scope.isOriginalSignedTermsConditnsSuccess = false;
                        });
            }else{
            //   $timeout(function () { 
            //         $scope.loading = false;
            //         $window.parent.location.reload(); }, 2000);
            }
        }
        
        $scope.submitOfficialReceiptDocuments = function (camp,userId) {
           console.log("campaign---",camp);
           let file = $scope.officialReceipt;
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                console.log("inside if---",file);
                let doc = file;
                let formData = new FormData();
                formData.append('file', doc);
                formData.append('sourceId', camp.campaignId);
                formData.append('sourceType', "Campaign");
                formData.append('userName', userId);
                formData.append('details',"Campaign_Payment");
                formData.append('docType', "Official_Receipt");
               
                $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
                    .success(function (response) {
                        console.log(response);
                        console.log(response.successful);
                         $scope.isOfficialReceiptSuccess = true;
                         $scope.isAPIcallsCompleted(camp,userId);
                    })
                    .error(function (error) {
                        console.log(error);
                         $scope.isOfficialReceiptSuccess = false;
                    });
            }else{
            //   $timeout(function () { 
            //         $scope.loading = false;
            //         $window.parent.location.reload(); }, 2000);
            }
        }
        
        $scope.submitAgencyLetterDocuments = function (camp,userId) {
           console.log("campaign---",camp);
           let file = $scope.agencyLetter;
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                    let doc = file;
                    let formData = new FormData();
                    formData.append('file', doc);
                    formData.append('sourceId', camp.campaignId);
                    formData.append('sourceType', "Campaign");
                    formData.append('userName', userId);
                    formData.append('details',"Campaign_Payment");
                    formData.append('docType', "Agency_Letter");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            $scope.isAgencyLetterSuccess = true;
                            $scope.isAPIcallsCompleted(camp,userId);
                        })
                        .error(function (error) {
                            console.log(error);
                            $scope.isAgencyLetterSuccess = false
                        });
            }else{
            //   $timeout(function () { 
            //         $scope.loading = false;
            //         $window.parent.location.reload(); }, 2000);
            }
        }
        
        $scope.submitSpecialPowerOfAttorneyDocuments = function (camp,userId) {
           console.log("campaign---",camp);
            let file = $scope.specialPowerofAttorney;
            console.log("file---",file);
            if (camp.campaignId && file) {
                var count = 0; 
                    console.log("inside if---",file);
                    let doc = file;
                    let formData = new FormData();
                    formData.append('file', doc);
                    formData.append('sourceId', camp.campaignId);
                    formData.append('sourceType', "Campaign");
                    formData.append('userName', userId);
                    formData.append('details',"Campaign_Payment");
                    formData.append('docType', "Special_Power_Of_Attorney");
                   
                    $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                        .success(function (response) {
                            console.log(response);
                            console.log(response.successful);
                            $scope.isSpecialPowerSuccess = true;
                            $scope.isAPIcallsCompleted(camp,userId);
                        })
                        .error(function (error) {
                            console.log(error);
                            $scope.isSpecialPowerSuccess = false;
                        });
            }else{
            //   $timeout(function () { 
            //         $scope.loading = false;
            //         $window.parent.location.reload(); }, 2000);
            }
        }
        
        $scope.submitTelecostDocuments = function (camp,userId) {
           console.log("campaign---",camp);
            
            let file = $scope.telecastOrder;
            if (camp.campaignId && file) {
               var count = 0; 
               console.log("inside if---",file);
               let doc = file;
               let formData = new FormData();
               formData.append('file', doc);
               formData.append('sourceId', camp.campaignId);
               formData.append('sourceType', "Campaign");
               formData.append('userName', userId);
               formData.append('details',"Campaign_Details");
               formData.append('docType', "Telecast_Order");
               
                $http.post(apiHostName+"/api/v1/s3/upload", formData, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
                    .success(function (response) {
                        console.log(response);
                        console.log(response.successful);
                        $scope.isTelecastorderSuccess = true;
                        $scope.isAPIcallsCompleted(camp,userId);
                    })
                    .error(function (error) {
                        console.log(error);
                        $scope.isTelecastorderSuccess = false;
                    });
            }else{
            //   $timeout(function () { 
            //         $scope.loading = false;
            //         $window.parent.location.reload();
            //   }, 2000);
            }
        }
        
        $scope.submitTask = function(camp,file,userId,buttonAction) {
            console.log('submit task ----')
            $scope.buttonAction = buttonAction;
            if($scope.polAdContract === undefined || $scope.polAdContract === '' || $scope.polAdContract === null) {
                $scope.isPolAdContrctError = true;
            } else {
                $scope.isPolAdContrctError = false;
                $scope.submitPolAdsContractDocuments(camp,userId);
            }
            //// 2
            if($scope.originalSignedTermsConditions === undefined || $scope.originalSignedTermsConditions === '' || $scope.originalSignedTermsConditions === null) {
                 $scope.isOriginalSignedTermsConditnsError = true;
   
            } else {
                $scope.isOriginalSignedTermsConditnsError = false;
                $scope.submitSignedTermsAndConditionsDocuments(camp,userId);
            }
            //// 3
            
            if($scope.officialReceipt === undefined || $scope.officialReceipt === '' || $scope.officialReceipt === null) {
                $scope.isOfficialReceiptError = true;
            } else {
                 $scope.isOfficialReceiptError = false;
                $scope.submitOfficialReceiptDocuments(camp,userId);
            }
            //// 4
            if($scope.agencyLetter === undefined || $scope.agencyLetter === '' || $scope.agencyLetter === null) {
                $scope.isAgencyLetterError = true;
            } else {
                $scope.isAgencyLetterError = false;
                $scope.submitAgencyLetterDocuments(camp,userId);
            }
            //// 5
            if($scope.specialPowerofAttorney === undefined || $scope.specialPowerofAttorney === '' || $scope.specialPowerofAttorney === null) {
                $scope.isSpecialPowerError = true;
            } else {
                $scope.isSpecialPowerError = false;
                $scope.submitSpecialPowerOfAttorneyDocuments(camp,userId);
            }
            ////6
            if($scope.telecastOrder === undefined || $scope.telecastOrder === '' || $scope.telecastOrder === null) {
                $scope.isTelecastorderError = true;
            } else {
                $scope.isTelecastorderError = false;
                $scope.submitTelecostDocuments(camp,userId);
            }
            // console.log('submit task')
            // $scope.submitSignedTermsAndConditionsDocuments(camp,userId);
            // $scope.submitOfficialReceiptDocuments(camp,userId);
            // $scope.submitAgencyLetterDocuments(camp,userId);
            // $scope.submitSpecialPowerOfAttorneyDocuments(camp,userId);
            // $scope.submitTelecostDocuments(camp,userId);
        }
        
        $scope.isAPIcallsCompleted = function(camp,userId){
            if($scope.isPolAdContrctSuccess &&
                $scope.isOriginalSignedTermsConditnsSuccess &&
                $scope.isOfficialReceiptSuccess &&
                $scope.isAgencyLetterSuccess &&
                $scope.isSpecialPowerSuccess &&
                $scope.isTelecastorderSuccess 
                ){
                    submitTaskAPI(camp,userId)
                }
        }
        function submitTaskAPI (camp,userId) {
        
            //console.log(data);
            var id;
            id = $scope.properties.taskId ;
            
             var data = {
                "userId": $scope.properties.userName,
                "buttonAction": $scope.buttonAction
            }
            if (id) {
                $http.put('../API/bpm/humanTask/' + id, { 'assigned_id': $scope.properties.userId }).then(function (response) {
                    //console.log(response, 'res task assign');
                    $http.post('../API/bpm/userTask/' + id + '/execution', data, { 'user': $scope.properties.userId }).then(function (response) {
                    }, function (error) {
                        //console.log(error);
                    });
    
                }, function (error) {
                    //console.log(error);
                });
    
    
            } else {
                console.log('Impossible to retrieve the task id value from the URL');
            }
        
        }
        $scope.callResponse = function() {
            if($scope.isPolAdContrctSuccess 
                && $scope.isOriginalSignedTermsConditnsSuccess
                && $scope.isOfficialReceiptSuccess
                && $scope.isAgencyLetterSuccess
                && $scope.isSpecialPowerSuccess
                && $scope.isTelecastorderSuccess) {
                    submitTask();
                }
        }
        
}