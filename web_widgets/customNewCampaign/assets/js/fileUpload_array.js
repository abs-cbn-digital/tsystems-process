angular.module("bonitasoft.ui.widgets").directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                console.log(scope.files,'begining');
                console.log(element[0].files, 'elements');
                if(scope.files && scope.files.length > 0){
                    //  if(scope.files && scope.files.length > 0){
                    //      alert("File limit exceeded");
                    //      return;
                    //  } else {
                         angular.forEach(scope.files, function (item) { 
                                 values.push(item);
                        });
                    //  }
                    
                }
                 console.log(element[0].files, 'elements after');
                angular.forEach(element[0].files, function (item) {
                    // var value = {
                    //   // File Name 
                    //     name: item.name,
                    //     //File Size 
                    //     size: item.size,
                    //     //File URL to view 
                    //     file: URL.createObjectURL(item),
                    //     // File Input Value 
                    //     _file: item
                    // };
                    //     console.log(item, 'minn');
                    //     console.log(scope.actionDocuments, 'docs4');
                    //  console.log(scope.files, 'files4');
                    values.push(item);
                });
                scope.$apply(function () {
                    console.log(values, 'apply');
                    if (isMultiple) {
                          scope.filesVar.append('file', values);
                        modelSetter(scope, values);
                    } else {
                                scope.filesVar.append('file',  values[0]);
                      //    scope.filesVar = values[0];
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);